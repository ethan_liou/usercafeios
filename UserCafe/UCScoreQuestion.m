//
//  UCScoreQuestion.m
//  UserCafeCore
//
//  Created by Liou Yu-Cheng on 13/3/4.
//  Copyright (c) 2013年 UserCafe. All rights reserved.
//

#import "UCScoreQuestion.h"
#import "UCStretchButton.h"
#import "UCContext.h"
@implementation UCScoreQuestion

@synthesize maxScores;

- (id)initWithDict:(NSDictionary*)dict WithOwner:(UCPageManager*)manager {
	self = [super initWithDict:dict WithOwner:manager];
	if (self) {
        NSLog(@"dict: %@", dict);
        leftLabel = [dict objectForKey:@"left_label"];
        rightLabel = [dict objectForKey:@"right_label"];
		self.maxScores = ((NSNumber*)[dict objectForKey:@"max_score"]).unsignedIntegerValue;
	}
	return self;
}

- (void)setScore:(NSInteger)score {
	NSInteger maxTag = score - 1;
	for (UCStretchButton * b in score_btns) {
		NSString * imgName = b.tag <= maxTag ? @"next_button_h" : @"button";
		[b setBackgroundNormalImage:[UCContext getImage:imgName]];
	}
}

- (void)clickScore:(UIButton*)btn {
	[self setScore:btn.tag + 1];
	self.answer = [NSString stringWithFormat:@"%d",btn.tag+1];
}

- (UIView *)renderContentView {
	// two lines block
	// 15 50 10 50 10 50 10 50 10 50 15
	// 15 10
	// 15 50
	UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50 + 10 + 50)];
    UILabel *left = [[UILabel alloc] initWithFrame:CGRectMake(14, 0, 240, 40)];
    left.backgroundColor = [UIColor clearColor];
    left.text = leftLabel;
    left.lineBreakMode = UILineBreakModeWordWrap;
    left.numberOfLines = 0;
    [left sizeToFit];
    [v addSubview:left];
    
    UILabel *right = [[UILabel alloc] initWithFrame:CGRectMake(274, 0, 40, 40)];
    right.backgroundColor = [UIColor clearColor];
    right.text = rightLabel;
    [right sizeToFit];
    [v addSubview:right];
	score_btns = [[NSMutableArray alloc] initWithCapacity:10];
	NSUInteger x_offset, y_offset;
	for(int i = 0 ; i < 2 ; i++){
		y_offset = i * 60 + left.frame.size.height;
		x_offset = 15;
		for (int j = 0 ; j < 5 ; j ++) {
			if(i*5+j >= maxScores)
				break;
			UCStretchButton *btn = [[UCStretchButton alloc] initWithFrame:CGRectMake(x_offset, y_offset, 50, 50) AndImage:[UCContext getImage:@"button"]];
			x_offset += 60;
			btn.tag = i*5+j;
			[btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
			[btn setTitle:[NSString stringWithFormat:@"%d", btn.tag+1] forState:UIControlStateNormal];
			[btn addTarget:self action:@selector(clickScore:) forControlEvents:UIControlEventTouchUpInside];
			[score_btns addObject:btn];
			[v addSubview:btn];
		}
	}
	if (self.answer != nil) {
		[self setScore:self.answer.integerValue];
	}
	return v;
}


@end
