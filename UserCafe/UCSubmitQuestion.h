//
//  UCSubmitQuestion.h
//  UserCafeCore
//
//  Created by Liou Yu-Cheng on 13/3/4.
//  Copyright (c) 2013年 UserCafe. All rights reserved.
//

#import "UCQuestion.h"

@interface UCSubmitQuestion : UCQuestion

- (id)initWithDict:(NSDictionary *)dict WithOwner:(UCPageManager *)manager;

@end
