//
//  main.m
//  TestUserCafe
//
//  Created by Liou Yu-Cheng on 13/3/18.
//  Copyright (c) 2013年 usercare. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
