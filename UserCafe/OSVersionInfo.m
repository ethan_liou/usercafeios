//
//  OSVersionInfo.m
//  UserCafe
//
//  Created by Liou Yu-Cheng on 2013/10/14.
//  Copyright (c) 2013年 usercare. All rights reserved.
//

#import "OSVersionInfo.h"
#import <UIKit/UIKit.h>

@implementation OSVersionInfo

-(NSString*) getName{
	return @"osVersion";
}

-(void) loadValue{
	self.value = [[UIDevice currentDevice] systemVersion];
	self.ready = YES;
}

@end
