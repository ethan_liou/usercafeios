//
//  UCSubmitQuestion.m
//  UserCafeCore
//
//  Created by Liou Yu-Cheng on 13/3/4.
//  Copyright (c) 2013年 UserCafe. All rights reserved.
//

#import "UCSubmitQuestion.h"
#import "UCStretchButton.h"
#import "UCConstant.h"
#import "UserCafe.h"
#import "UCContext.h"

@implementation UCSubmitQuestion

- (id)initWithDict:(NSDictionary*)dict WithOwner:(UCPageManager*)manager{
	self = [super initWithDict:dict WithOwner:manager];
	if (self) {
		self.title = UCLocalizedString(@"finish", @"填寫完成");
		if(manager != nil && manager.owner != nil && manager.owner.owner != nil && manager.owner.owner.submitText != nil)
			self.desc = manager.owner.owner.submitText;
	}
	return self;
}

- (void)sendToGoogle{
	[[NSNotificationCenter defaultCenter] postNotificationName:NOTI_SENDPAGE object:nil];
}

- (UIView *)renderContentView {
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    UCStretchButton *button;
    if (UIInterfaceOrientationIsPortrait(orientation)) {
        button = [[UCStretchButton alloc] initWithFrame:CGRectMake(20, 0, 280, 44) AndImage:[UCContext getImage:@"button"]];
    } else {
        CGRect screenBounds = [[UIScreen mainScreen] bounds];
        if (screenBounds.size.height == 480) {
            button = [[UCStretchButton alloc] initWithFrame:CGRectMake(40, 0, 400, 44) AndImage:[UCContext getImage:@"button"]];
        } else {
            button = [[UCStretchButton alloc] initWithFrame:CGRectMake(84, 0, 400, 44) AndImage:[UCContext getImage:@"button"]];
        }
    }
	[button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	[button setTitle:UCLocalizedString(@"submit", @"送出") forState:UIControlStateNormal];
	[button addTarget:self action:@selector(sendToGoogle) forControlEvents:UIControlEventTouchUpInside];
	return button;
}

- (UILabel *)addAuthLabel {
    UILabel *authLabel = [[UILabel alloc] initWithFrame: CGRectMake(0, 100, 320, 44)];
    authLabel.text = @"Powered by User Cafe";
    authLabel.textColor = [UIColor colorWithRed:120/255.0 green:194/255.0 blue:196/255.0 alpha:1.0];
    [authLabel setFont:[UIFont fontWithName:@"Heiti TC" size:20]];
    authLabel.textAlignment = NSTextAlignmentCenter; // 置中
    [authLabel setBackgroundColor:[UIColor clearColor]];
    return authLabel;
}


@end
