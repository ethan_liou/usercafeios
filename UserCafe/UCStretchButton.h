//
//  UCStretchButton.h
//  UserCafeCore
//
//  Created by Liou Yu-Cheng on 13/3/10.
//  Copyright (c) 2013年 UserCafe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UCStretchButton : UIButton

- (id)initWithFrame:(CGRect)frame AndImage:(UIImage*)image;
- (void)setBackgroundNormalImage:(UIImage *)image;

@end
