//
//  UCDateQuestion.h
//  UserCafe
//
//  Created by 威威 on 13/7/16.
//  Copyright (c) 2013年 usercare. All rights reserved.
//
#import "UCQuestion.h"
#import "UCConstant.h"

@protocol DateQuestionDelegate <NSObject>
- (void)showTimePicker:(NSInteger)type;
@end

@interface UCDateQuestion : UCQuestion <UIGestureRecognizerDelegate> {
}

- (id)initWithDict:(NSDictionary *)dict WithOwner:(UCPageManager *)manager;
// 0 : 年月日, 1 : 年月日+時間, 2 : 月日, 3 : 月日+時間
@property (nonatomic) NSInteger dateType;
@property (weak, nonatomic) IBOutlet UIButton *timeBtn;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (nonatomic, strong) id<DateQuestionDelegate> delegate;
@end
