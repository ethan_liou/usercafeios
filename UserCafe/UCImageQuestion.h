//
//  UCImageQuestion.h
//  UserCafe
//
//  Created by 威威 on 13/7/30.
//  Copyright (c) 2013年 usercare. All rights reserved.
//

#import "UCQuestion.h"

@interface UCImageQuestion : UCQuestion
- (id)initWithDict:(NSDictionary *)dict WithOwner:(UCPageManager *)manager;
@end
