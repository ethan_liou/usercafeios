//
//  LocationInfo.m
//  UserCafe
//
//  Created by Liou Yu-Cheng on 2013/10/14.
//  Copyright (c) 2013年 usercare. All rights reserved.
//

#import "LocationInfo.h"

@implementation LocationInfo

-(NSString*) getName{
	return @"location";
}

-(void) loadValue{
	locationManager = [[CLLocationManager alloc] init];
	locationManager.delegate = self;
	locationManager.distanceFilter = kCLDistanceFilterNone;
	locationManager.desiredAccuracy = kCLLocationAccuracyBest;
	[locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
	[locationManager stopUpdatingLocation];
    NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
	self.value = [NSString stringWithFormat:@"%f,%f",newLocation.coordinate.latitude, newLocation.coordinate.longitude];
	self.ready = YES;
}



@end
