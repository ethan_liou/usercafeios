//
//  UCUtils.m
//  UserCafeCore
//
//  Created by Liou Yu-Cheng on 13/3/4.
//  Copyright (c) 2013年 UserCafe. All rights reserved.
//

#import "UCUtils.h"
#import <CommonCrypto/CommonDigest.h>
#import "HTMLUtils.h"

// mac
#include <sys/socket.h> // Per msqr
#include <sys/sysctl.h>
#include <net/if.h>
#include <net/if_dl.h>

@implementation UCUtils

+(NSString*)getUDID{
	NSString *ver = [[UIDevice currentDevice] systemVersion];
    CGFloat ver_float = [ver floatValue];
	NSString *udid;
	if (ver_float >= 6.0f)
		udid = [UIDevice currentDevice].identifierForVendor.UUIDString;
	else
		udid = @"Unknown";
	return udid;
}

+(NSData *)decrypt:(NSData *)data withKey:(NSString *)key{
	NSMutableData *result = [data mutableCopy];
    char *dataPtr = (char *) [result mutableBytes];
    char *keyData = (char *) [[key dataUsingEncoding:NSUTF8StringEncoding] bytes];
	
    char *keyPtr = keyData;
    int keyIndex = 0;
	
    for (int x = 0; x < [data length]; x++)
    {
        *dataPtr = *dataPtr ^ *keyPtr;
		dataPtr++;
		keyPtr++;
		
        if (++keyIndex == [key length])
            keyIndex = 0, keyPtr = keyData;
    }
	
    return result;
}

//+(NSDictionary*)loadDictFromDecryptFile:(NSString *)path{
//	NSData * data = [NSData dataWithContentsOfFile:path];
//	NSData * decData = [UCUtils decrypt:data withKey:@"S"];
//	SBJsonParser * parser = [[SBJsonParser alloc] init];
//	NSDictionary * dict = [parser objectWithData:decData];
//	return dict;
//}

+ (UIImage *)loadStretchImage:(UIImage*)image{
	NSInteger top = 6;
	NSInteger left = 6;
	NSInteger bottom = 9;
	NSInteger right = 7;
	
	UIEdgeInsets edge = UIEdgeInsetsMake(top, left, bottom, right);
	
	return [image resizableImageWithCapInsets:edge];
}

+(NSString*)calculateMd5ByData:(NSData *)data{
	unsigned char result[16];
    CC_MD5( data.bytes, data.length, result ); // This is the md5 call
    return [NSString stringWithFormat:
			@"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
			result[0], result[1], result[2], result[3],
			result[4], result[5], result[6], result[7],
			result[8], result[9], result[10], result[11],
			result[12], result[13], result[14], result[15]
			];
}

+(NSString*)calculateMd5ByPath:(NSString *)path{
	NSData * data = [NSData dataWithContentsOfFile:path];
	return [UCUtils calculateMd5ByData:data];
}

+(NSString*)getMacAddress{
	int                 mib[6];
	size_t              len;
	char                *buf;
	unsigned char       *ptr;
	struct if_msghdr    *ifm;
	struct sockaddr_dl  *sdl;
	
	mib[0] = CTL_NET;
	mib[1] = AF_ROUTE;
	mib[2] = 0;
	mib[3] = AF_LINK;
	mib[4] = NET_RT_IFLIST;
	
	if ((mib[5] = if_nametoindex("en0")) == 0) {
		printf("Error: if_nametoindex error\n");
		return NULL;
	}
	
	if (sysctl(mib, 6, NULL, &len, NULL, 0) < 0) {
		printf("Error: sysctl, take 1\n");
		return NULL;
	}
	
	if ((buf = malloc(len)) == NULL) {
		printf("Could not allocate memory. error!\n");
		return NULL;
	}
	
	if (sysctl(mib, 6, buf, &len, NULL, 0) < 0) {
		printf("Error: sysctl, take 2");
		free(buf);
		return NULL;
	}
	
	ifm = (struct if_msghdr *)buf;
	sdl = (struct sockaddr_dl *)(ifm + 1);
	ptr = (unsigned char *)LLADDR(sdl);
	NSString *outstring = [NSString stringWithFormat:@"%02X:%02X:%02X:%02X:%02X:%02X",
						   *ptr, *(ptr+1), *(ptr+2), *(ptr+3), *(ptr+4), *(ptr+5)];
	free(buf);
	
	return outstring;
}

# pragma mark - post

+(NSDictionary*)loadPostDataRequestTo:(NSURL*)url WithDict:(NSDictionary*)dict{
	NSString * qs = [HTMLUtils dictToString:dict];
	
	NSMutableURLRequest *postRequest = [NSMutableURLRequest requestWithURL:url];
	
	// Set the request's content type to application/x-www-form-urlencoded
	[postRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
	
	// Designate the request a POST request and specify its body data
	[postRequest setHTTPMethod:@"POST"];
	[postRequest setHTTPBody:[qs dataUsingEncoding:NSUTF8StringEncoding]];
	
	NSError *error = nil;
	NSHTTPURLResponse *responseCode = nil;
	
	NSData * jsonData = [NSURLConnection sendSynchronousRequest: postRequest returningResponse: &responseCode error: &error];
	if(error || (responseCode.statusCode != 200 && responseCode.statusCode != 204)){
		// [TODO] error handling
		NSLog(@"load error %@",error);
		return nil;
	}
	if(responseCode.statusCode == 204){
		// already filled
		return [NSDictionary dictionary];
	}
	NSDictionary * contentDict = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:&error];
	if(error){
		// [TODO] error handling
		NSLog(@"parse json error %@",error);
		return nil;
	}
	return contentDict;
}

+(BOOL)sendPostRequestTo:(NSURL*)url WithDict:(NSDictionary*)dict{
	NSString * qs = [HTMLUtils dictToString:dict];
	
	NSMutableURLRequest *postRequest = [NSMutableURLRequest requestWithURL:url];
	
	// Set the request's content type to application/x-www-form-urlencoded
	[postRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
	
	// Designate the request a POST request and specify its body data
	[postRequest setHTTPMethod:@"POST"];
	[postRequest setHTTPBody:[qs dataUsingEncoding:NSUTF8StringEncoding]];
	
	NSError *error = nil;
	NSHTTPURLResponse *responseCode = nil;
	
	[NSURLConnection sendSynchronousRequest:postRequest returningResponse:&responseCode error:&error];
	if(error){
		return NO;
	}
	return responseCode.statusCode == 200;
}


# pragma mark - base64

static const char _base64EncodingTable[64] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
static const short _base64DecodingTable[256] = {
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -1, -1, -2, -1, -1, -2, -2,
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
	-1, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, 62, -2, -2, -2, 63,
	52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -2, -2, -2, -2, -2, -2,
	-2,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
	15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -2, -2, -2, -2, -2,
	-2, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
	41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -2, -2, -2, -2, -2,
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
	-2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2
};

+ (NSString *) base64EncodeString: (NSString *) strData {
	return [self base64EncodeData: [strData dataUsingEncoding: NSUTF8StringEncoding] ];
}

+ (NSString *) base64EncodeData: (NSData *) objData {
	const unsigned char * objRawData = [objData bytes];
	char * objPointer;
	char * strResult;
	
	// Get the Raw Data length and ensure we actually have data
	int intLength = [objData length];
	if (intLength == 0) return nil;
	
	// Setup the String-based Result placeholder and pointer within that placeholder
	strResult = (char *)calloc(((intLength + 2) / 3) * 4, sizeof(char));
	objPointer = strResult;
	
	// Iterate through everything
	while (intLength > 2) { // keep going until we have less than 24 bits
		*objPointer++ = _base64EncodingTable[objRawData[0] >> 2];
		*objPointer++ = _base64EncodingTable[((objRawData[0] & 0x03) << 4) + (objRawData[1] >> 4)];
		*objPointer++ = _base64EncodingTable[((objRawData[1] & 0x0f) << 2) + (objRawData[2] >> 6)];
		*objPointer++ = _base64EncodingTable[objRawData[2] & 0x3f];
		
		// we just handled 3 octets (24 bits) of data
		objRawData += 3;
		intLength -= 3;
	}
	
	// now deal with the tail end of things
	if (intLength != 0) {
		*objPointer++ = _base64EncodingTable[objRawData[0] >> 2];
		if (intLength > 1) {
			*objPointer++ = _base64EncodingTable[((objRawData[0] & 0x03) << 4) + (objRawData[1] >> 4)];
			*objPointer++ = _base64EncodingTable[(objRawData[1] & 0x0f) << 2];
			*objPointer++ = '=';
		} else {
			*objPointer++ = _base64EncodingTable[(objRawData[0] & 0x03) << 4];
			*objPointer++ = '=';
			*objPointer++ = '=';
		}
	}
	
	// Terminate the string-based result
	*objPointer = '\0';
	
	// Return the results as an NSString object
	return [NSString stringWithCString:strResult encoding:NSASCIIStringEncoding];
}

+ (NSData *) base64DecodeString: (NSString *) strBase64 {
	const char * objPointer = [strBase64 cStringUsingEncoding:NSASCIIStringEncoding];
	int intLength = strlen(objPointer);
	int intCurrent;
	int i = 0, j = 0, k;
	
	unsigned char * objResult;
	objResult = calloc(intLength, sizeof(char));
	
	// Run through the whole string, converting as we go
	while ( ((intCurrent = *objPointer++) != '\0') && (intLength-- > 0) ) {
		if (intCurrent == '=') {
			if (*objPointer != '=' && ((i % 4) == 1)) {// || (intLength > 0)) {
				// the padding character is invalid at this point -- so this entire string is invalid
				free(objResult);
				return nil;
			}
			continue;
		}
		
		intCurrent = _base64DecodingTable[intCurrent];
		if (intCurrent == -1) {
			// we're at a whitespace -- simply skip over
			continue;
		} else if (intCurrent == -2) {
			// we're at an invalid character
			free(objResult);
			return nil;
		}
		
		switch (i % 4) {
			case 0:
				objResult[j] = intCurrent << 2;
				break;
				
			case 1:
				objResult[j++] |= intCurrent >> 4;
				objResult[j] = (intCurrent & 0x0f) << 4;
				break;
				
			case 2:
				objResult[j++] |= intCurrent >>2;
				objResult[j] = (intCurrent & 0x03) << 6;
				break;
				
			case 3:
				objResult[j++] |= intCurrent;
				break;
		}
		i++;
	}
	
	// mop things up if we ended on a boundary
	k = j;
	if (intCurrent == '=') {
		switch (i % 4) {
			case 1:
				// Invalid state
				free(objResult);
				return nil;
				
			case 2:
				k++;
				// flow through
			case 3:
				objResult[k] = 0;
		}
	}
	
	// Cleanup and setup the return NSData
	NSData * objData = [[NSData alloc] initWithBytes:objResult length:j] ;
	free(objResult);
	return objData;
}


@end
