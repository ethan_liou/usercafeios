//
//  ViewController.h
//  UserCafe
//
//  Created by Liou Yu-Cheng on 13/3/20.
//  Copyright (c) 2013年 usercare. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "UserCafe.h"
#import "AdWhirlDelegateProtocol.h"

@interface ViewController : UIViewController <UserCafeDelegate, AdWhirlDelegate>
@property (strong, nonatomic) AppDelegate *appDelegate;
@end
