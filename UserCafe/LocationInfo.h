//
//  LocationInfo.h
//  UserCafe
//
//  Created by Liou Yu-Cheng on 2013/10/14.
//  Copyright (c) 2013年 usercare. All rights reserved.
//

#import "AbstractDeviceInfo.h"
#import <CoreLocation/CoreLocation.h>

@interface LocationInfo : AbstractDeviceInfo<CLLocationManagerDelegate>{
	CLLocationManager *locationManager;
}

@end
