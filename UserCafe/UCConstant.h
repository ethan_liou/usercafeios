//
//  UCConstant.h
//  UserCafeCore
//
//  Created by Liou Yu-Cheng on 13/3/12.
//  Copyright (c) 2013年 UserCafe. All rights reserved.
//

#ifndef UserCafeCore_UCConstant_h
#define UserCafeCore_UCConstant_h

#define SERVER_URL @"http://usercafe.com"
#define SERVER_NEW_URL @"http://usercafe.com/api/app"

#define NOTI_NEXTPAGE @"nextpage"
#define NOTI_JUMPPAGE @"jumppage"
#define NOTI_SENDPAGE @"sendpage"

#define TEXT_TAG_OFFSET 1000
#define DATE_TAG 2000
#define TIME_TAG 3000

// mode
#define PAGE_MODE_SUBMIT 0
#define PAGE_MODE_MID 1

#endif
