//
//  DeviceInfoFactory.m
//  UserCafe
//
//  Created by Liou Yu-Cheng on 2013/10/14.
//  Copyright (c) 2013年 usercare. All rights reserved.
//

#import "DeviceInfoFactory.h"
#import "AbstractDeviceInfo.h"
#import "CarrierNameInfo.h"
#import "CarrierNumberInfo.h"
#import "DeviceModelInfo.h"
#import "CellIdInfo.h"
#import "LocationInfo.h"
#import "OSVersionInfo.h"
#import "WifiBSSIDInfo.h"

@implementation DeviceInfoFactory

+(NSMutableArray*)generateInfoWithNames:(NSArray*)names{
	NSArray * definedInfos = [NSArray arrayWithObjects:[[DeviceModelInfo alloc] init],[[CarrierNameInfo alloc] init],[[CarrierNumberInfo alloc] init],[[CellIdInfo alloc] init],[[LocationInfo alloc] init], [[OSVersionInfo alloc] init], [[WifiBSSIDInfo alloc] init], nil];
	NSMutableArray * output = [[NSMutableArray alloc] init];
	for (NSString * name in names) {
		for (AbstractDeviceInfo * info in definedInfos) {
			if ([name.lowercaseString isEqualToString:[info getName].lowercaseString]) {
				[output addObject:info];
				break;
			}
		}
	}
	return output;
}

@end
