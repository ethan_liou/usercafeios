//
//  CarrierNumberInfo.m
//  UserCafe
//
//  Created by Liou Yu-Cheng on 2013/10/14.
//  Copyright (c) 2013年 usercare. All rights reserved.
//

#import "CarrierNumberInfo.h"
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>

@implementation CarrierNumberInfo

-(NSString*) getName{
	return @"carrierNumber";
}

-(void) loadValue{
	CTTelephonyNetworkInfo *netinfo = [[CTTelephonyNetworkInfo alloc] init];
	CTCarrier *carrier = [netinfo subscriberCellularProvider];
	self.value = [NSString stringWithFormat:@"%@,%@",
				  carrier.mobileCountryCode == nil ? @"Unknown" : carrier.mobileCountryCode ,
				  carrier.mobileNetworkCode == nil ? @"Unknown" : carrier.mobileNetworkCode];
	self.ready = YES;
}

@end
