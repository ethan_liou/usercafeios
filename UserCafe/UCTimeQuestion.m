//
//  UCTimeQuestion.m
//  UserCafe
//
//  Created by 威威 on 13/7/16.
//  Copyright (c) 2013年 usercare. All rights reserved.
//

#import "UCTimeQuestion.h"
#import "UCConstant.h"
#import <QuartzCore/QuartzCore.h>

@implementation UCTimeQuestion
@synthesize delegate, duration;
- (IBAction)timeBtn:(id)sender {
    if (duration) {
        [delegate showTimePicker:3];
    } else {
        [delegate showTimePicker:2];
    }
}
- (IBAction)timeBtnLand:(id)sender {
    if (duration) {
        [delegate showTimePicker:3];
    } else {
        [delegate showTimePicker:2];
    }
}

- (id)initWithDict:(NSDictionary *)dict WithOwner:(UCPageManager *)manager
{
	self = [super initWithDict:dict WithOwner:manager];
	if (self) {
        type = [[dict objectForKey:@"time_type"] integerValue]; // 0 : 一般時間, 1 : Duration
        if (type == 0) {
            duration = NO;
        } else {
            duration = YES;
        }
	}
	return self;
}

- (UIView *)renderContentView
{
    UIView *rootView;
    UIInterfaceOrientation orientation;
    orientation = [UIApplication sharedApplication].statusBarOrientation;
    if(orientation == UIInterfaceOrientationPortrait) {
        rootView = [[[UCContext getBundle] loadNibNamed:@"UCTimeQuestion" owner:self options:nil] objectAtIndex:0];
    }
    else if(orientation == UIInterfaceOrientationLandscapeRight || orientation == UIInterfaceOrientationLandscapeLeft) {
        CGRect screenBounds = [[UIScreen mainScreen] bounds];
        if (screenBounds.size.height == 480) {
            rootView = [[[UCContext getBundle] loadNibNamed:@"UCTimeQuestionLand" owner:self options:nil] objectAtIndex:0];
        } else {
            rootView = [[[UCContext getBundle] loadNibNamed:@"UCTimeQuestionLand4" owner:self options:nil] objectAtIndex:0];
        }
    }
    rootView.backgroundColor = [UIColor colorWithRed:227.0/255.0 green:227.0/255.0 blue:227.0/255.0 alpha:1];
    if (self.answer) {
        if (duration) {
            NSArray *cut = [self.answer componentsSeparatedByString:@":"];
            NSString *h = [cut objectAtIndex:0];
            NSString *m = [cut objectAtIndex:1];
            NSString *s = [cut objectAtIndex:2];
            self.timeLabel.text = [NSString stringWithFormat:@"%@ 小時, %@ 分, %@ 秒", h, m, s];
        } else {
            self.timeLabel.text = self.answer;
        }
    } else {
        if (duration) {
            self.timeLabel.text = @"累積時間";
        } else {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            NSDate *date = [NSDate date];
            [formatter setDateFormat:@"KK:mm aa"];
            self.timeLabel.text = [formatter stringFromDate:date];
        }
    }
    UITapGestureRecognizer *tapTimeRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleUILabelTimeTap:)];
    tapTimeRecognizer.numberOfTapsRequired = 1;
    tapTimeRecognizer.numberOfTouchesRequired = 1;
    tapTimeRecognizer.cancelsTouchesInView = NO;
    tapTimeRecognizer.delegate = self;
    self.timeLabel.userInteractionEnabled = YES;
    [self.timeLabel addGestureRecognizer:tapTimeRecognizer];
    return rootView;
}

- (void)handleUILabelTimeTap:(UITapGestureRecognizer *)recognizer {
    [self timeBtn:Nil];
}
@end
