//
//  DeviceModelInfo.m
//  UserCafe
//
//  Created by Liou Yu-Cheng on 2013/10/14.
//  Copyright (c) 2013年 usercare. All rights reserved.
//

#import "DeviceModelInfo.h"
#import <UIKit/UIKit.h>

@implementation DeviceModelInfo

-(NSString*) getName{
	return @"deviceModel";
}

-(void) loadValue{

	self.value = [UIDevice currentDevice].model;
	self.ready = YES;
}

@end
