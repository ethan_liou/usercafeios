//
//  UCImageQuestion.m
//  UserCafe
//
//  Created by 威威 on 13/7/30.
//  Copyright (c) 2013年 usercare. All rights reserved.
//

#import "UCImageQuestion.h"
#import <Foundation/Foundation.h>

@implementation UCImageQuestion

NSData *base64Data;

- (id)initWithDict:(NSDictionary *)dict WithOwner:(UCPageManager *)manager {
    self = [super initWithDict:dict WithOwner:manager];
	if (self) {
        NSString *img_data = [dict objectForKey:@"img_data"];
        NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"data:image/jpeg;base64,%@", img_data] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        base64Data = [NSData dataWithContentsOfURL:url];
	}
    return self;
}

- (UIView *)renderContentView {
    UIImageView *imageView;
    UIInterfaceOrientation orientation;
    orientation = [UIApplication sharedApplication].statusBarOrientation;
    if(orientation == UIInterfaceOrientationPortrait) {
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 320)];
    }
    else if(orientation == UIInterfaceOrientationLandscapeRight || orientation == UIInterfaceOrientationLandscapeLeft) {
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 480, 480)];
    }
    imageView.image = [UIImage imageWithData:base64Data];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    return imageView;
}
@end
