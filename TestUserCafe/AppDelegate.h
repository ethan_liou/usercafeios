//
//  AppDelegate.h
//  TestUserCafe
//
//  Created by Liou Yu-Cheng on 13/3/18.
//  Copyright (c) 2013年 usercare. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserCafe.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) UserCafe *userCafe;
@property (strong, nonatomic) UIViewController *viewController;
@property (strong, nonatomic) UIWindow *window;
@end
