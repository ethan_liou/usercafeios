//
//  UCStretchTextField.m
//  UserCafeCore
//
//  Created by Liou Yu-Cheng on 13/3/11.
//  Copyright (c) 2013年 UserCafe. All rights reserved.
//

#import "UCStretchTextField.h"
#import "UCUtils.h"
#import "UCContext.h"

@implementation UCStretchTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

- (CGRect)textRectForBounds:(CGRect)bounds {
	return CGRectInset( bounds , 10 , 10 );
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
	return CGRectInset( bounds , 10 , 10 );
}

- (id)initWithCoder:(NSCoder *)aDecoder{
	self = [super initWithCoder:aDecoder];
    if (self) {
		
		[self setBackground:[UCUtils loadStretchImage:[UCContext getImage:@"button"]]];
	}
    return self;
	
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
