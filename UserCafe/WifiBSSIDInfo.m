//
//  WifiBSSIDInfo.m
//  UserCafe
//
//  Created by Liou Yu-Cheng on 2013/10/14.
//  Copyright (c) 2013年 usercare. All rights reserved.
//

#import "WifiBSSIDInfo.h"

@implementation WifiBSSIDInfo

-(NSString*) getName{
	return @"wifiBSSID";
}

-(void) loadValue{
	self.ready = NO;
}

@end
