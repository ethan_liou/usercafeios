//
//  UCTimeQuestion.h
//  UserCafe
//
//  Created by 威威 on 13/7/16.
//  Copyright (c) 2013年 usercare. All rights reserved.
//

#import "UCQuestion.h"

#define kHourComponent 0
#define kMinuteComponent 1
#define kSecondComponent 2

@protocol TimeQuestionDelegate <NSObject>
- (void)showTimePicker:(NSInteger)type;
@end

@interface UCTimeQuestion : UCQuestion <UIGestureRecognizerDelegate> {
    NSInteger type;
}
- (id)initWithDict:(NSDictionary *)dict WithOwner:(UCPageManager *)manager;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (nonatomic, strong) id<TimeQuestionDelegate> delegate;
@property (nonatomic) bool duration;
@end
