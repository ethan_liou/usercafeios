//
//  CarrierNameInfo.m
//  UserCafe
//
//  Created by Liou Yu-Cheng on 2013/10/14.
//  Copyright (c) 2013年 usercare. All rights reserved.
//

#import "CarrierNameInfo.h"
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>

@implementation CarrierNameInfo

-(NSString*) getName{
	return @"carrierName";
}

-(void) loadValue{
	CTTelephonyNetworkInfo *netinfo = [[CTTelephonyNetworkInfo alloc] init];
	CTCarrier *carrier = [netinfo subscriberCellularProvider];
	self.value = [carrier carrierName];
	self.ready = YES;
}

@end
