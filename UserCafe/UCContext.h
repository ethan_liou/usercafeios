//
//  UCContext.h
//  UserCafe
//
//  Created by Liou Yu-Cheng on 13/5/14.
//  Copyright (c) 2013年 usercare. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface UCContext : NSObject

#define UCLocalizedString(key, comment) \
[[UCContext getBundle] localizedStringForKey:(key) value:@"" table:nil]

@property (nonatomic,strong) NSString * formKey;
@property (nonatomic,strong) NSString * account;
@property (nonatomic,strong) NSString * submitText;
@property (nonatomic,strong) NSDictionary * customData;

// device related
+ (NSBundle*)getBundle;
+ (UIImage*)getImage:(NSString*)name;
+ (BOOL)loadBundle:(NSString*)bundleName;
+ (void)setSdkKey:(NSString*)key;
+ (NSString*)getSdkKey;
+ (NSString*)getDeviceId;
+ (UIView*)renderADBannerWithFrame:(CGRect)frame;
+ (void)clearCacheWithKey:(NSString*)key;

// context related
+ (void)openQuestionnaire;


@end
