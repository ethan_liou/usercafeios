//
//  UCStretchButton.m
//  UserCafeCore
//
//  Created by Liou Yu-Cheng on 13/3/10.
//  Copyright (c) 2013年 UserCafe. All rights reserved.
//

#import "UCStretchButton.h"
#import "UCUtils.h"

@implementation UCStretchButton

- (void)setBackgroundNormalImage:(UIImage *)image{
	[self setBackgroundImage:[UCUtils loadStretchImage:image] forState:UIControlStateNormal];
}

- (id)initWithCoder:(NSCoder *)aDecoder{
	self = [super initWithCoder:aDecoder];
    if (self) {
		[self setBackgroundImage:[UCUtils loadStretchImage:[self backgroundImageForState:UIControlStateNormal]] forState:UIControlStateNormal];
	}
    return self;

}

- (id)initWithFrame:(CGRect)frame AndImage:(UIImage*)image
{
    self = [super initWithFrame:frame];
    if (self) {
		[self setBackgroundImage:[UCUtils loadStretchImage:image] forState:UIControlStateNormal];
	}
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
