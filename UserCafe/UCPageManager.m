//
//  UCPageManager.m
//  UserCafeCore
//
//  Created by Liou Yu-Cheng on 13/3/4.
//  Copyright (c) 2013年 UserCafe. All rights reserved.
//

#import "UCPageManager.h"
#import "UCQuestion.h"
#import "UCQuestionFactory.h"
#import "UCGridQuestion.h"
#import "UCCheckboxQuestion.h"
#import "UCMultipleChoiceQuestion.h"
#import "UCConstant.h"
#import "HTMLUtils.h"

@implementation UCPageManager

@synthesize pageNum, defaultNextPageNum, mode;
@synthesize jumpIdx, jumpPageNum, questions;
@synthesize hidden, nextPageNum, prevPageNum, defaultActionUrl, googleVer;
@synthesize owner;

- (NSString *)description{
	return [NSString stringWithFormat:@"%d %d %@",self.pageNum,self.nextPageNum, self.questions];
}

- (NSString *)cacheRelatedKey{
	switch (googleVer) {
		case 1:
			return @"backupCache";
		case 2:
			return @"draftResponse";
		default:
			break;
	}
	return @"";
}

- (NSString *)pageRelatedKey{
	switch (googleVer) {
		case 1:
			return @"pageNumber";
		case 2:
			return @"pageHistory";
		default:
			break;
	}
	return @"";
}

- (NSString *)getOtherParam{
	switch(googleVer){
		case 1:
			return @"other_option_";
		case 2:
			return @"other_option_response";
		default:
			break;
	}
	return @"";
}

- (id)initWithDict:(NSDictionary *)json AndPageNum:(NSInteger)page AndVer:(NSInteger)ver
{
	self = [super init];
	if (self)
    {
		self.pageNum = page;
		self.defaultNextPageNum = ((NSNumber *)[json objectForKey:@"default_next_page"]).integerValue;
		self.mode = ((NSNumber *)[json objectForKey:@"mode"]).integerValue;
		self.jumpIdx = ((NSNumber *)[json objectForKey:@"jump_idx"]).integerValue;
		self.jumpPageNum = [[NSArray alloc] initWithArray:[json objectForKey:@"jump_page"]];
		googleVer = ver;
		
		// load pages，一頁裡問題的陣列
		NSArray * questionArr = (NSArray *)[json objectForKey:@"questions"];
		self.questions = [[NSMutableArray alloc] init];
        NSString *level_1 = nil;
        NSString *level_2 = nil;
        // NSString *level_3 = nil;
		for(NSDictionary * question in questionArr){
			NSUInteger index = [questionArr indexOfObject:question];
			UCQuestion * q = [UCQuestionFactory createQuestionWithDict:question WithOwner:self];
			if (q != nil) {
				// set some info
				if(index == 0)
					q.firstQuestion = YES;
				else if(index == questionArr.count - 1)
					q.lastQuestion = YES;
				if(index == self.jumpIdx && q.type == 2) {	// multiple choice
					((UCMultipleChoiceQuestion *)q).jumpPage = YES;
				}
                if (q.type == 7) {
                    level_1 = [q.title stringByReplacingOccurrencesOfString:@" " withString:@""];
                    q.firstLayer = level_1;
                    level_2 = nil;
                    // level_3 = nil;
                } else if (q.type == 8) {
                    level_2 = [q.title stringByReplacingOccurrencesOfString:@" " withString:@""];
                    q.firstLayer = level_1;
                    q.secondLayer = level_2;
                    // level_3 = nil;
                } else {
                    q.firstLayer = level_1;
                    q.secondLayer = level_2;
                    // q.thirdLayer = level_3;
                }
				if(q.type == 6){ // grid [TODO] defined
					[(UCGridQuestion *)q addFirstPageAndMultipleQuestionToArr:self.questions];
				}
				else{
                    [self.questions addObject:q];
				}
			}
		}
	}
	return self;
}

- (NSComparisonResult)compare:(UCPageManager *)otherObject {
    return self.pageNum - otherObject.pageNum;
}

- (NSString *)encode:(NSString*)str{
	//	return [str stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
	return str;
}

- (NSString *)handleName:(NSString*)str{
	return [str stringByReplacingOccurrencesOfString:@"." withString:@"_"];
}

- (NSString *)getOtherAnswer{
	switch (self.googleVer) {
		case 1:
			return @"__option__";
		case 2:
			return @"__other_option__";
			break;
		default:
			break;
	}
	return @"";
}


- (NSDictionary *)getAnswerDict {
	NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
	for (UCQuestion * q in self.questions) {
		if (q.answer != nil) {
			// other
			if(q.useOther) {
				q.answer = [q.answer stringByReplacingOccurrencesOfString:[self getOtherAnswer] withString:q.otherString];
			}
			[dict setObject:q.answer forKey:[self handleName:q.name]];
            if (q.timer) {
                [dict setObject:[NSString stringWithFormat:@"%d", q.timer] forKey:[NSString stringWithFormat:@"%@_t", [self handleName:q.name]]];
            }
		}
	}
	return dict;
}

@end