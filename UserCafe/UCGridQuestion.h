//
//  UCGridQuestion.h
//  UserCafeCore
//
//  Created by Liou Yu-Cheng on 13/3/4.
//  Copyright (c) 2013年 UserCafe. All rights reserved.
//

#import "UCQuestion.h"

@interface UCGridQuestion : UCQuestion{
	NSArray * scoreLabels;
	NSArray * rowLabels;
	NSArray * names;	
}

-(id)initWithDict:(NSDictionary*)dict WithOwner:(UCPageManager*)manager;
-(void)addFirstPageAndMultipleQuestionToArr:(NSMutableArray*)array;

@end
