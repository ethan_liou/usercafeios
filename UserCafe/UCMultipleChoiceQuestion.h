//
//  UCMultipleChoiceQuestion.h
//  UserCafeCore
//
//  Created by Liou Yu-Cheng on 13/3/4.
//  Copyright (c) 2013年 UserCafe. All rights reserved.
//

#import "UCQuestion.h"

@interface UCMultipleChoiceQuestion : UCQuestion <UITableViewDataSource,UITableViewDelegate, UIAlertViewDelegate>
{
	NSIndexPath * checkedIndexPath;
	UITableView * tableView_;
	UITextField * otherTextField_;
}

@property (nonatomic) BOOL jumpPage;
@property (nonatomic,strong) NSArray * options;

- (void)setNextPage;
- (id)initWithTitle:(NSString *)title AndName:(NSString *)name AndDesc:(NSString *)desc AndRequired:(BOOL)required AndOptions:(NSArray *)options;
- (id)initWithDict:(NSDictionary *)dict WithOwner:(UCPageManager *)manager;

@end
