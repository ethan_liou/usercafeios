//
//  UCQuestionFactory.m
//  UserCafeCore
//
//  Created by Liou Yu-Cheng on 13/3/4.
//  Copyright (c) 2013年 UserCafe. All rights reserved.
//

#import "UCQuestionFactory.h"
#import "UCQuestion.h"
#import "UCTextQuestion.h"
#import "UCMultipleChoiceQuestion.h"
#import "UCCheckboxQuestion.h"
#import "UCScoreQuestion.h"
#import "UCGridQuestion.h"
#import "UCPageBreakerQuestion.h"
#import "UCSubmitQuestion.h"
#import "UCDateQuestion.h"
#import "UCTimeQuestion.h"
#import "UCImageQuestion.h"

@implementation UCQuestionFactory

+ (UCQuestion *)createQuestionWithDict:(NSDictionary*)dict WithOwner:(UCPageManager*)manager
{
	UCQuestionType type = ((NSNumber *)[dict objectForKey:@"type"]).integerValue;
	UCQuestion *question = nil;
	switch (type) {
		case UCQuestionTypeText:
		case UCQuestionTypeLongText:
			question = [[UCTextQuestion alloc] initWithDict:dict WithOwner:manager];
			break;
		case UCQuestionTypeMultipleChoice:
		case UCQuestionTypeList:
			question = [[UCMultipleChoiceQuestion alloc] initWithDict:dict WithOwner:manager];
			break;
		case UCQuestionTypeCheckbox:
			question = [[UCCheckboxQuestion alloc] initWithDict:dict WithOwner:manager];
			break;
		case UCQuestionTypeScale:
			question = [[UCScoreQuestion alloc] initWithDict:dict WithOwner:manager];
			break;
		case UCQuestionTypeGrid:
			question = [[UCGridQuestion alloc] initWithDict:dict WithOwner:manager];
			break;
		case UCQuestionTypePageBreaker:
		case UCQuestionTypeSectionHeader:
			question = [[UCPageBreakerQuestion alloc] initWithDict:dict WithOwner:manager];
			break;
		case UCQuestionTypeSubmit:
			question = [[UCSubmitQuestion alloc] initWithDict:dict WithOwner:manager];
			break;
        case UCQuestionTypeImage:
            question = [[UCImageQuestion alloc] initWithDict:dict WithOwner:manager];
            break;
        case UCQuestionTypeDate:
            question = [[UCDateQuestion alloc] initWithDict:dict WithOwner:manager];
            break;
        case UCQuestionTypeTime:
            question = [[UCTimeQuestion alloc] initWithDict:dict WithOwner:manager];
            break;
		default:
			break;
	}
	return question;
}

@end
