//
//  UCScoreQuestion.h
//  UserCafeCore
//
//  Created by Liou Yu-Cheng on 13/3/4.
//  Copyright (c) 2013年 UserCafe. All rights reserved.
//

#import "UCQuestion.h"

@interface UCScoreQuestion : UCQuestion {
    NSString *leftLabel;
    NSString *rightLabel;
	NSMutableArray * score_btns;
}

@property (nonatomic) NSUInteger maxScores;

- (id)initWithDict:(NSDictionary *)dict WithOwner:(UCPageManager*)manager;

@end
