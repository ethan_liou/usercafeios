//
//  UCQuestionViewController.m
//  UserCafeCore
//
//  Created by Liou Yu-Cheng on 13/3/4.
//  Copyright (c) 2013年 UserCafe. All rights reserved.
//

#import "UCQuestionViewController.h"

@interface UCQuestionViewController ()
@end

@implementation UCQuestionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)setFrame:(CGRect)rect {
	[self.view setFrame:rect];
	[scrollView setFrame:CGRectMake(0, 0, rect.size.width, rect.size.height - 16)];
}

- (void)set:(UIView *)view belowView:(UIView *)targetView WithSpace:(NSInteger)space
{
	CGFloat y = targetView.frame.origin.y;
	if(!targetView.hidden)y += targetView.frame.size.height;
	CGRect viewRect = view.frame;
	[view setFrame:CGRectMake(viewRect.origin.x, y + space, viewRect.size.width, viewRect.size.height)];
}
// 把 question 嵌到畫面裡面
- (NSInteger)renderQuestion:(UCQuestion *)question
{
	NSInteger SPACE = 5;
    if (question.title) {
		titleLabel.hidden = NO;
		titleLabel.text = question.title;
        if (question.required) {
			// change size
			requiredLabel.hidden = NO;
			if(titleLabel.frame.origin.x == requiredLabel.frame.origin.x)
				titleLabel.frame = CGRectMake(requiredLabel.frame.origin.x + requiredLabel.frame.size.width, requiredLabel.frame.origin.y, titleLabel.frame.size.width - requiredLabel.frame.size.width, titleLabel.frame.size.height);
        } else {
			requiredLabel.hidden = YES;
			if(titleLabel.frame.origin.x != requiredLabel.frame.origin.x)
				titleLabel.frame = CGRectMake(requiredLabel.frame.origin.x, requiredLabel.frame.origin.y, titleLabel.frame.size.width + requiredLabel.frame.size.width, titleLabel.frame.size.height);
        }
		[titleLabel sizeToFit];
    } else {
        titleLabel.hidden = YES;
		requiredLabel.hidden = YES;
    }
	descLabel.text = question.desc;
	[descLabel sizeToFit];
	[self set:descLabel belowView:titleLabel WithSpace:SPACE];
	contentView = [question renderContentView];
	[self set:contentView belowView:descLabel WithSpace:SPACE];
	[scrollView addSubview:contentView];
	[scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, contentView.frame.origin.y + contentView.frame.size.height)];
    return contentView.frame.origin.y;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
