//
//  UCCheckboxQuestion.h
//  UserCafeCore
//
//  Created by Liou Yu-Cheng on 13/3/4.
//  Copyright (c) 2013年 UserCafe. All rights reserved.
//

#import "UCQuestion.h"

@interface UCCheckboxQuestion : UCQuestion<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>{
	NSMutableSet * checkSet;
	UITextField * otherTextField_;
	NSIndexPath * otherIndexPath_;
	UITableView * tableView_;
}

@property (nonatomic,strong) NSArray * options;

-(id)initWithDict:(NSDictionary*)dict WithOwner:(UCPageManager*)manager;

@end
