//
//  UCUtils.h
//  UserCafeCore
//
//  Created by Liou Yu-Cheng on 13/3/4.
//  Copyright (c) 2013年 UserCafe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface UCUtils : NSObject

//+(NSDictionary*)loadDictFromDecryptFile:(NSString*)filename;
+(UIImage *)loadStretchImage:(UIImage*)image;
+(NSData *)decrypt:(NSData *)data withKey:(NSString *)key;
+(NSString*)calculateMd5ByData:(NSData*)data;
+(NSString*)calculateMd5ByPath:(NSString*)path;
+ (NSData *) base64DecodeString: (NSString *) strBase64 ;
+(NSString*)getMacAddress;
+(NSString*)getUDID;
+(NSDictionary*)loadPostDataRequestTo:(NSURL*)url WithDict:(NSDictionary*)dict;
+(BOOL)sendPostRequestTo:(NSURL*)url WithDict:(NSDictionary*)dict;
@end
