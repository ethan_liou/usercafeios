	//
//  UCViewController.m
//  UserCafeCore
//
//  Created by Liou Yu-Cheng on 13/3/4.
//  Copyright (c) 2013年 UserCafe. All rights reserved.
//

#import "UCMainViewController.h"
#import "UCQuestion.h"
#import "UCQuestionViewController.h"
#import "UCPageManager.h"
#import "UCConstant.h"
#import "UCTextQuestion.h"
#import "UCStretchTextField.h"
#import "UCSubmitQuestion.h"
#import "UCPageBreakerQuestion.h"
#import "UCDateQuestion.h"
#import "UCTimeQuestion.h"
#import "UCUtils.h"
#import "UCMultipleChoiceQuestion.h"
#import "UCAnswerCache.h"
#import <DeviceInfoFactory.h>
#import <AbstractDeviceInfo.h>
#import <QuartzCore/QuartzCore.h>

#define KEY_SURVEY_ID   @"_survey_id"
#define KEY_APP_ID      @"_app_id"
#define KEY_DEVICE_ID   @"_device_id"
#define KEY_FINISH_TIME @"_finish_time"
#define KEY_FINISHED    @"_finished"

@interface UCMainViewController () <DateQuestionDelegate, TimeQuestionDelegate> {
    float ver_float;
	NSMutableArray *questionArr;
	NSInteger previousPage;
	NSInteger visiblePageCount;
	UCAnswerCache *cache;
    IBOutlet UIView *loadingV;
	IBOutlet UIImageView *loadingBGIV;
	IBOutlet UILabel *textL;
    BOOL textFieldAnimation;
    NSInteger pickerType;
    // For timer.
    NSTimer *timer;
    NSInteger totalSecond;
    NSInteger singleSecond;
    // Resizing.
    UIInterfaceOrientation orientation;
    UITextField *focusView;
    NSInteger movingDistance;
    UCDateQuestion *dateQ;
    UCTimeQuestion *timeQ;
    UILabel *currentDateLabel;
    UILabel *currentTimeLabel;
	BOOL isFinish;
	NSMutableArray * deviceInfos;
}

- (BOOL)parseJson;
- (NSDictionary *)loadJsonFromKey:(NSString *)key;

@end

@implementation UCMainViewController

@synthesize cacheKey, surveyKey, delegate, queue, owner, titleLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		self.queue = dispatch_queue_create("com.usercafe.view", NULL);
	}
	return self;
}

- (IBAction)backBtn
{
	UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:UCLocalizedString(@"notice", @"Error")
                                                        message:UCLocalizedString(@"exit", @"此題為必答題")
                                                       delegate:self
                                              cancelButtonTitle:UCLocalizedString(@"cancel", @"cancel")
                                              otherButtonTitles:UCLocalizedString(@"ok", @"OK"), nil];
	alertView.tag = UCAlertTypeExit;
	[alertView show];
}
- (BTBreadcrumbItem *)item:(NSString *)title {
    BTBreadcrumbItem *item = [[BTBreadcrumbItem alloc] init];
    item.title = title;
    return item;
}
// Only call on currentQ.
- (void)adjustUI:(UCQuestion *)q {
	if ([q isKindOfClass:[UCTextQuestion class]]) {
		UITextField *ft = (UITextField *)[self.view viewWithTag:TEXT_TAG_OFFSET + q.pos];
        if(q.answer != nil)
            ft.text = q.answer;
        focusView = ft;
        focusView.delegate = self;
	} else {
        if (focusView) {
            [focusView resignFirstResponder];
            focusView = nil;
        }
    }
    if ([q isKindOfClass:[UCDateQuestion class]]) {
        dateQ = (UCDateQuestion *)q;
        dateQ.delegate = self;
        if (dateQ.dateType == 0 || dateQ.dateType == 2) {
            // only date.
            currentDateLabel = (UILabel *)[self.view viewWithTag:DATE_TAG];
        } else {
            currentDateLabel = (UILabel *)[self.view viewWithTag:DATE_TAG];
            currentTimeLabel = (UILabel *)[self.view viewWithTag:TIME_TAG];
        }
    } else if ([q isKindOfClass:[UCTimeQuestion class]]) {
        timeQ = (UCTimeQuestion *)q;
        timeQ.delegate = self;
        currentTimeLabel = (UILabel *)[self.view viewWithTag:TIME_TAG];
    }
}

- (BOOL)parseJson {
	NSDictionary * pages = [questionJson objectForKey:@"pages"];
	actionUrl = [questionJson objectForKey:@"action_url"];
	googleDocVer = ((NSNumber *)[questionJson objectForKey:@"version"]).integerValue;
	pageNoToPageManagerDict = [[NSMutableDictionary alloc] initWithCapacity:[pages count]];
	questionArr = [[NSMutableArray alloc] init];
	// Device info.
	if([questionJson objectForKey:@"properties"] != nil){
		NSDictionary * properties = [questionJson objectForKey:@"properties"];
		if([properties objectForKey:@"device_info"] != nil){
			NSArray * names = [properties objectForKey:@"device_info"];
			deviceInfos = [DeviceInfoFactory generateInfoWithNames:names];
		}
	}
	for(AbstractDeviceInfo * info in deviceInfos){
		[info loadValue];
	}
    titleLabel.text = [questionJson objectForKey:@"title"];
	// title & desc.
	UCPageBreakerQuestion *titleQuestion = [[UCPageBreakerQuestion alloc] initWithTitle:nil AndDesc:[questionJson objectForKey:@"desc"] AndRequired:NO];
	[questionArr addObject:titleQuestion];
	NSArray * keys = [pages allKeys];
	NSArray * sortedKeys = [keys sortedArrayUsingComparator:^(NSString* obj1, NSString* obj2){
		return [obj1 caseInsensitiveCompare:obj2];
	}];
    // Sorting 過的頁數
	for (NSString * pageNum in sortedKeys) {
		UCPageManager *manager = [[UCPageManager alloc] initWithDict:[pages objectForKey:pageNum] AndPageNum:pageNum.integerValue AndVer:googleDocVer];
		manager.defaultActionUrl = actionUrl;
		[pageNoToPageManagerDict setObject:manager forKey:pageNum];
		[questionArr addObjectsFromArray:manager.questions]; // 把以頁為單位的 question 倒入 questionArr
	}
	// Load answer from cache.
	for (UCQuestion * q in questionArr) {
		q.answer = [cache loadCacheByName:q.name];
        q.timer = [[cache loadCacheByName:[NSString stringWithFormat:@"%@.t", q.name]] integerValue];
	}
	return YES;
}

- (IBAction)navBtnClick:(id)sender {
    NSInteger direction = sender == _prevBtn ? -1 : 1;
	NSInteger width = _scrollView.frame.size.width;
	NSInteger newPage = previousPage + direction;
	if(newPage >= 0 && newPage < questionArr.count) {
        if (newPage > previousPage) {
            UCQuestion *q = [questionArr objectAtIndex:previousPage];
            if (q.required && q.answer == nil) {
				if(!isDialogOpen) {
					isDialogOpen = YES;
					UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:UCLocalizedString(@"notice", @"Error")
                                                                        message:UCLocalizedString(@"required", @"此題為必答題")
                                                                       delegate:self
                                                              cancelButtonTitle:UCLocalizedString(@"ok", @"OK")
                                                              otherButtonTitles:nil];
					alertView.tag = UCAlertTypeRequired;
					[alertView show];
				}
			} else {
                [_scrollView setContentOffset:CGPointMake(newPage * width, 0) animated:YES];
            }
        } else {
            [_scrollView setContentOffset:CGPointMake(newPage * width, 0) animated:YES];
        }
    }
}

- (BOOL)loadJson:(NSDictionary *)json
{
	// load answer cache
	cache = [[UCAnswerCache alloc] initWithMd5:cacheKey];
	questionJson = json;
	return [self parseJson];
}

- (void)renderScrollView
{
	NSInteger width = _scrollView.frame.size.width;
	NSInteger height = _scrollView.frame.size.height;
	
	for (UIView *view in _scrollView.subviews) {
		if (view.frame.origin.x >= width * previousPage){
			[view removeFromSuperview];
		}
	}
	
	visiblePageCount = 0;
	for (UCQuestion * q in questionArr) {
		visiblePageCount += q.show ? 1 : 0;
	}
	[_scrollView setContentSize:CGSizeMake(visiblePageCount * width, height)];
	NSUInteger offset = 0;
    NSInteger position = 0;
	for (UCQuestion *q in questionArr) {
		if (!q.show) {
            q.realPos = -1;
			continue;
		}
		q.realPos = offset;
		q.pos = [questionArr indexOfObject:q];
		UCQuestionViewController *tv = [[UCQuestionViewController alloc] initWithNibName:@"UCQuestionViewController" bundle:[UCContext getBundle]];
		[tv setFrame:CGRectMake(width * offset, 16, width, height)];
        position = [tv renderQuestion:q];
		[_scrollView addSubview:tv.view];
        if ([q isKindOfClass:[UCTextQuestion class]]) {
            q.position = position;
        }
        /*
		if (offset == previousPage) {
            [self adjustUI:q];
		}
         */
		offset++;
	}
}

- (NSDictionary *)loadJsonFromKey:(NSString *)key {
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/survey/%@/get_survey",SERVER_NEW_URL, key, surveyKey]];
	NSDictionary *contentDict = [UCUtils loadPostDataRequestTo:url WithDict:[NSDictionary dictionaryWithObject:[UCUtils getUDID] forKey:@"_device_id"]];
	
//	+(NSDictionary*)loadPostDataRequestTo:(NSURL*)url WithDict:(NSDictionary*)dict {
	
//    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:
//                              ];
//	NSURLResponse *response = nil;
//	NSData *jsonData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//	if (error) {
//		// [TODO] error handling
//		return nil;
//	}
//	NSDictionary *contentDict = [NSJSONSerialization JSONObjectWithData:jsonData
//                                                                options:NSJSONReadingAllowFragments
//                                                                  error:&error];
//	if (error) {
//		return nil;
//	}
	// Set context.
	if (contentDict != nil && [contentDict count] != 0) {
		owner.account = [contentDict objectForKey:@"account"];
		owner.formKey = [contentDict objectForKey:@"form_key"];
	}
	return contentDict;
}

- (void)hideLoading {
	[loadingV setHidden:YES];
}

- (void)showLoading:(NSString *)text {
	[loadingV setHidden:NO];
	[textL setText:text];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    // Check iOS version
    NSString *ver = [[UIDevice currentDevice] systemVersion];
    NSLog(@"System Version is %@", ver);
    ver_float = [ver floatValue];
    if (ver_float < 7.0) {
        [_navView setFrame:CGRectMake(0, 0, screenBounds.size.width, 44)];
        CGRect frame = _progress.frame;
        frame.origin.y = 39.0f;
        _progress.frame = frame;
        
        frame = _navBackBtn.frame;
        frame.origin.y = 0.0f;
        _navBackBtn.frame = frame;
        
        frame = titleLabel.frame;
        frame.origin.y = 0.0f;
        titleLabel.frame = frame;
        
        _breadcrumb = [[BTBreadcrumbView alloc] initWithFrame:CGRectMake(-2, 48, 0, 0)];
        _breadcrumb.delegate = self;
        [_breadcrumb sizeToFit];
        [self.view addSubview:_breadcrumb];
    } else {
        _breadcrumb = [[BTBreadcrumbView alloc] initWithFrame:CGRectMake(-2, 66, 0, 0)];
        _breadcrumb.delegate = self;
        [_breadcrumb sizeToFit];
        [self.view addSubview:_breadcrumb];
    }
	isFinish = YES;
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(secondCount:) userInfo:nil repeats:YES];
	previousPage = 0;
	[_progress setProgressTintColor:[UIColor colorWithRed:0x38/255.0f green:0x8E/255.0f blue:0x7D/255.0f alpha:1.0f]];
	[_progress setTrackTintColor:[UIColor colorWithRed:0xC4/255.0f green:0xD6/255.0f blue:0xD3/255.0f alpha:1.0f]];
    [loadingBGIV setImage:[[UCContext getImage:@"loading_bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)]];
    orientation = [UIApplication sharedApplication].statusBarOrientation;
    if (orientation != UIInterfaceOrientationPortrait) {
        loadingV.center = CGPointMake(screenBounds.size.height/2, 100);
    }
	[_navView setImage:[UCContext getImage:@"navigation_bar"]];
	[_navBackBtn setImage:[UCContext getImage:@"icon_back"] forState:UIControlStateNormal];
	[_prevBtn setHidden:YES];
	[_prevBtn setTitle:UCLocalizedString(@"prev", nil) forState:UIControlStateNormal];
	[_nextBtn setHidden:YES];
	[_nextBtn setTitle:UCLocalizedString(@"next", nil) forState:UIControlStateNormal];
    [self showLoading:UCLocalizedString(@"loading", @"loading")];
	dispatch_async(self.queue, ^{
		NSDictionary *dict = [self loadJsonFromKey:cacheKey];
        surveyKey = [dict objectForKey:@"_id"];
        // NSLog(@"dict: %@", dict);
		dispatch_async(dispatch_get_main_queue(), ^{
			[self hideLoading];
			if (dict != nil) {
				if(dict.count == 0){
					if(delegate){
						[delegate UserCafeFailedWithError:UCFilled];
					}
					[self dismissViewControllerAnimated:YES completion:nil];
				}
				else{
					[self loadJson:dict];
					[self renderScrollView];
					[_nextBtn setHidden:NO];
				}
			}
			else {
				if (delegate) {
					[delegate UserCafeFailedWithError:UCNetworkError];
				}
				[self dismissViewControllerAnimated:YES completion:nil];
			}
		});
	});
    self.authLable.hidden = YES;
    self.authImg.hidden = YES;
    // 收鍵盤的手勢
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    tapRecognizer.numberOfTapsRequired = 1;
    tapRecognizer.numberOfTouchesRequired = 1;
    tapRecognizer.cancelsTouchesInView = NO;
    tapRecognizer.delegate = self;
    [self.view addGestureRecognizer:tapRecognizer];
}
// 每秒呼叫一次
- (void)secondCount:(NSTimer *)sender {
    totalSecond++;
    singleSecond++;
}

- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(detectDeviceOrientation)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(nextPage)
                                                 name:NOTI_NEXTPAGE
                                               object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(jumpPageCallback:)
                                                 name:NOTI_JUMPPAGE
                                               object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sendAnswer)
                                                 name:NOTI_SENDPAGE
                                               object:nil];
}

- (void)dealloc {
	dispatch_release(self.queue);
}

- (void)hideFromPage:(NSUInteger)fromPage ToPage:(NSUInteger)toPage {
	// Show from & to, hide mid.
	BOOL change = NO;
	for (UCQuestion *q in questionArr) {
		if (q.owner.pageNum == fromPage || q.owner.pageNum == toPage) {
			// show
			if (!q.show) {
				change = YES;
				q.show = YES;
			}
		} else if(q.owner.pageNum > fromPage && q.owner.pageNum < toPage){
			// hide
			if (q.show) {
				change = YES;
				q.show = NO;
			}
		}
	}
	if (change) {
		[self renderScrollView];
	}
}

- (void)jumpPageCallback:(NSNotification *)notification {
	if (notification != nil && notification.object != nil && [notification.object isKindOfClass:[NSArray class]])
    {
		NSArray *array = (NSArray *)notification.object;
		[self hideFromPage:((NSNumber *)[array objectAtIndex:0]).unsignedIntegerValue
					ToPage:((NSNumber *)[array objectAtIndex:1]).unsignedIntegerValue];
	}
}

- (void)insertNecessaryArguments:(NSMutableDictionary *)params{
	// device info
	if(deviceInfos != nil){
		for(AbstractDeviceInfo * info in deviceInfos){
			[params setObject:[info getValue] forKey:[NSString stringWithFormat:@"_%@",[info getName]]];
		}
	}
	
	// custom
	if(owner.customData != nil){
		[params addEntriesFromDictionary:owner.customData];
	}
	// finished
	[params setObject:isFinish?@"true":@"false" forKey:KEY_FINISHED];
	
    // timer
    if (totalSecond)
        [params setObject:[NSString stringWithFormat:@"%d", totalSecond] forKey:KEY_FINISH_TIME];
    [params setObject:surveyKey forKey:KEY_SURVEY_ID];
    [params setObject:[UCUtils getUDID] forKey:KEY_DEVICE_ID]; // 要改掉
    [params setObject:cacheKey forKey:KEY_APP_ID];
}
static BOOL sending = NO;
- (void)sendAnswer {
	NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
	for(UCPageManager *pm in pageNoToPageManagerDict.objectEnumerator){
		if (!pm.hidden) {
			[dict addEntriesFromDictionary:[pm getAnswerDict]];
		}
	}
	[self insertNecessaryArguments:dict];
    NSLog(@"sending: %d", sending);
	if(!sending) {
		sending = YES;
		[self showLoading:UCLocalizedString(@"sending", @"sending")];
		dispatch_async(self.queue, ^{
			BOOL ret = [UCUtils sendPostRequestTo:
                        [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/survey/%@", SERVER_NEW_URL, cacheKey, surveyKey]] WithDict:dict];
			dispatch_async(dispatch_get_main_queue(), ^{
				[self hideLoading];
				sending = NO;
				if (delegate) {
					if (ret) {
						// clear cache
						// [UCContext clearCacheWithKey:cacheKey];
						if (isFinish)
							[delegate UserCafeSuccess];
					}
					else if(isFinish)
						[delegate UserCafeFailedWithError:UCNetworkError];
				}
				[self dismissViewControllerAnimated:YES completion:nil];
			});
		});
	}
}

- (void)nextPage {
	NSInteger width = _scrollView.frame.size.width;
	NSInteger newPage = previousPage + 1;
	if(newPage >= 0 && newPage < questionArr.count)
		[_scrollView setContentOffset:CGPointMake(newPage * width, 0) animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
	if (cache != nil) {
        [timer invalidate];
        [cache saveCacheWithName:surveyKey AndAnswer:[NSString stringWithFormat:@"%d", totalSecond]];
		[cache commit];
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma Scroll related

BOOL isDialogOpen = NO;
// Find current Q.
UCQuestion *currentQ = nil;
NSInteger page;
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    float fractionalPage = scrollView.contentOffset.x / pageWidth;
    page = lround(fractionalPage);
    [self performSelector:@selector(scrollViewDidEndScrollingAnimation:)
                   withObject:nil
                   afterDelay:0.3];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    if (previousPage != page && page >= 0 && page < [questionArr count]) {
        if (previousPage < page && previousPage != 0) {
            UCQuestion *q = [questionArr objectAtIndex:previousPage];
            NSString *qNameTimer = [NSString stringWithFormat:@"%@.t", q.name];
            q.timer = singleSecond;
            [cache saveCacheWithName:qNameTimer AndAnswer:[NSString stringWithFormat:@"%d", q.timer]];
            singleSecond = 0;
        }
        // Find prev q.
        for (UCQuestion *q in questionArr) {
            if (q.answer != nil){
                [cache saveCacheWithName:q.name AndAnswer:q.answer];
            }
            if (q.realPos == previousPage && q.required && q.answer == nil && previousPage < page) {
                // Need to answer
                if (!isDialogOpen) {
                    isDialogOpen = YES;
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:UCLocalizedString(@"notice", @"Error") message:UCLocalizedString(@"required", @"此題為必答題") delegate:self cancelButtonTitle:UCLocalizedString(@"ok", @"OK") otherButtonTitles:nil];
                    alertView.tag = UCAlertTypeRequired;
                    [alertView show];
                }
                return;
            }
        }
        NSInteger idx = 0;
        for (UCQuestion *q in questionArr) {
            if (q.realPos == page) {
                currentQ = q;
                break;
            }
            idx++;
        }
        [_prevBtn setHidden:idx == 0];
        if ([currentQ isKindOfClass:[UCSubmitQuestion class]]) {
            [_nextBtn setHidden:YES];
            self.authLable.hidden = NO;
            self.authImg.hidden = NO;
        } else{
            [_nextBtn setHidden:NO];
            self.authLable.hidden = YES;
            self.authImg.hidden = YES;
        }
        if(currentQ != nil && [currentQ isKindOfClass:[UCMultipleChoiceQuestion class]]){
            // check jump page
            UCMultipleChoiceQuestion *mcq = (UCMultipleChoiceQuestion *)currentQ;
            [mcq setNextPage];
        }
        previousPage = page;
        CGFloat progress = 1.0*(page + 1)/visiblePageCount;
        [_progress setProgress:progress animated:YES];
    }
    
    if ((currentQ.firstLayer == nil && currentQ.secondLayer == nil) || currentQ.type == 9) {
        [_breadcrumb setItems:[NSArray arrayWithObjects:nil]
                     animated:YES];
    } else {
        [self handleBreadcrumb:currentQ.firstLayer AndSection:currentQ.secondLayer AndGrid:currentQ.thirdLayer];
    }
    [self adjustUI:currentQ];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (orientation == UIInterfaceOrientationPortrait) {
        CGRect screenBounds = [[UIScreen mainScreen] bounds];
        if (screenBounds.size.height == 480) {
            if (130 < currentQ.position && currentQ.position < 220) {
                textFieldAnimation = YES;
                movingDistance = 60;
                [UIView beginAnimations:nil context:NULL];
                [UIView setAnimationDuration:0.3];
                [UIView setAnimationDelegate:self];
                // 設定動畫開始時的狀態為目前畫面上的樣子
                [UIView setAnimationBeginsFromCurrentState:YES];
                _scrollView.frame = CGRectMake(_scrollView.frame.origin.x,
                                               _scrollView.frame.origin.y - movingDistance,
                                               _scrollView.frame.size.width,
                                               _scrollView.frame.size.height);
                [UIView commitAnimations];
            } else if (currentQ.position >= 220) {
                textFieldAnimation = YES;
                movingDistance = 180;
                [UIView beginAnimations:nil context:NULL];
                [UIView setAnimationDuration:0.3];
                [UIView setAnimationDelegate:self];
                // 設定動畫開始時的狀態為目前畫面上的樣子
                [UIView setAnimationBeginsFromCurrentState:YES];
                _scrollView.frame = CGRectMake(_scrollView.frame.origin.x,
                                               _scrollView.frame.origin.y - movingDistance,
                                               _scrollView.frame.size.width,
                                               _scrollView.frame.size.height);
                [UIView commitAnimations];
            } else {
                textFieldAnimation = NO;
            }
        } else {
            if (currentQ.position > 250) {
                textFieldAnimation = YES;
                movingDistance = 120;
                [UIView beginAnimations:nil context:NULL];
                [UIView setAnimationDuration:0.3];
                [UIView setAnimationDelegate:self];
                // 設定動畫開始時的狀態為目前畫面上的樣子
                [UIView setAnimationBeginsFromCurrentState:YES];
                _scrollView.frame = CGRectMake(_scrollView.frame.origin.x,
                                               _scrollView.frame.origin.y - movingDistance,
                                               _scrollView.frame.size.width,
                                               _scrollView.frame.size.height);
                [UIView commitAnimations];
            } else {
                textFieldAnimation = NO;
            }
        }
    } else {
        if (currentQ.position > 80) {
            textFieldAnimation = YES;
            movingDistance = 100;
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.3];
            [UIView setAnimationDelegate:self];
            [UIView setAnimationBeginsFromCurrentState:YES];
            _scrollView.frame = CGRectMake(_scrollView.frame.origin.x,
                                           _scrollView.frame.origin.y - movingDistance,
                                           _scrollView.frame.size.width,
                                           _scrollView.frame.size.height);
            [UIView commitAnimations];
        } else {
            textFieldAnimation = NO;
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textFieldAnimation) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationBeginsFromCurrentState:YES];
        _scrollView.frame = CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y + movingDistance, _scrollView.frame.size.width, _scrollView.frame.size.height);
        [UIView commitAnimations];
    }
}

- (void)handleTap:(UITapGestureRecognizer *)recognizer {
    if (focusView && (![focusView isExclusiveTouch])) {
        [focusView resignFirstResponder];
    }
}

- (void)handleBreadcrumb:(NSString *)level_1 AndSection:(NSString *)level_2 AndGrid:(NSString *)level_3 {
    if (level_1 == nil) {
        if (level_2 == nil) {
            if (level_3 == nil) {
            } else {
                // level_3 only
                [_breadcrumb setItems:[NSArray arrayWithObjects:[self item:level_3], nil] animated:YES];
            }
        } else {
            if (level_3 == nil) {
                // level_2 only
                [_breadcrumb setItems:[NSArray arrayWithObjects:[self item:level_2], nil] animated:YES];
            } else {
                NSArray *breadcrumb = [self shortenBreadcrumb:level_2 section:level_3 grid:nil];
                [_breadcrumb setItems:[NSArray arrayWithObjects:[self item:[breadcrumb objectAtIndex:0]], [self item:[breadcrumb objectAtIndex:1]], nil]animated:YES];
            }
        }
        
    } else {
        if (level_2 == nil) {
            if (level_3 == nil) {
                // level_1 only
                [_breadcrumb setItems:[NSArray arrayWithObjects:[self item:level_1], nil] animated:YES];
            } else {
                NSArray *breadcrumb = [self shortenBreadcrumb:level_1 section:level_3 grid:nil];
                [_breadcrumb setItems:[NSArray arrayWithObjects:[self item:[breadcrumb objectAtIndex:0]], [self item:[breadcrumb objectAtIndex:1]], nil]animated:YES];
            }
        } else {
            if (level_3 == nil) {
                NSArray *breadcrumb = [self shortenBreadcrumb:level_1 section:level_2 grid:nil];
                [_breadcrumb setItems:[NSArray arrayWithObjects:[self item:[breadcrumb objectAtIndex:0]], [self item:[breadcrumb objectAtIndex:1]], nil]animated:YES];
            } else {
                [_breadcrumb setItems:[NSArray arrayWithObjects:[self item:level_1], [self item:level_2], [self item:level_3], nil] animated:YES];
            }
        }
    }
}

- (NSArray *)shortenBreadcrumb:(NSString *)pageBreaker section:(NSString *)sectionHeader grid:(NSString *)gridTitle {
    if (gridTitle == nil) {
        NSInteger sectionLength = sectionHeader.length;
        NSInteger retainSize;
        if (sectionLength < 14) {
            retainSize = 18 - sectionLength;
            if (pageBreaker.length <= retainSize) {
                // do nothing
            } else {
                NSString *header = [pageBreaker substringToIndex:retainSize - 2];
                NSString *tail = [pageBreaker substringFromIndex:pageBreaker.length - 1];
                pageBreaker = [NSString stringWithFormat:@"%@...%@", header, tail];
            }
        } else if (sectionLength == 14) {
            retainSize = 4;
            if (pageBreaker.length <= retainSize) {
                // do nothing
            } else {
                NSString *header = [pageBreaker substringToIndex:2];
                NSString *tail = [pageBreaker substringFromIndex:pageBreaker.length - 1];
                pageBreaker = [NSString stringWithFormat:@"%@...%@", header, tail];
            }
        } else if (sectionLength > 14) {
            if (pageBreaker.length <= 4) {
                retainSize = 18 - pageBreaker.length;
                NSString *header = [sectionHeader substringToIndex:retainSize - 2];
                NSString *tail = [sectionHeader substringFromIndex:sectionLength - 1];
                sectionHeader = [NSString stringWithFormat:@"%@...%@", header, tail];
            } else {
                retainSize = 14;
                NSString *header = [pageBreaker substringToIndex:2];
                NSString *tail = [pageBreaker substringFromIndex:pageBreaker.length - 1];
                pageBreaker = [NSString stringWithFormat:@"%@...%@", header, tail];
                
                NSString *sHeader = [sectionHeader substringToIndex:retainSize - 2];
                NSString *sTail = [sectionHeader substringFromIndex:sectionLength - 1];
                sectionHeader = [NSString stringWithFormat:@"%@...%@", sHeader, sTail];
            }
        }
        NSArray *temp = [[NSArray alloc] initWithObjects:pageBreaker, sectionHeader, nil];
        return temp;
    } else {
        return nil;
    }
}
#pragma mark DateQuestionDelegate
#define kDatePickerTag 100
#define kTimeDurationPickerTag 101
#define SelectButtonIndex 1
#define SelectButtonWidth 280

- (void)showTimePicker:(NSInteger)type {
    // 0, 1, 2
    pickerType = type;
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"Select" forState:UIControlStateNormal];
    btn.showsTouchWhenHighlighted = YES;
    [btn setBackgroundColor:[UIColor colorWithRed:102.0/255.0 green:186.0/255.0 blue:183.0/255.0 alpha:1.0]];
    if (pickerType != 3) {
        if (orientation == UIInterfaceOrientationPortrait) {
            blur = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
            customPickerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 216 - 44, 320, 260)];
            datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, 320, 216)];
            [customPickerView addSubview:datePicker];
            btn.frame = CGRectMake(0, 216, 320, 44);
            [btn addTarget:self action:@selector(timeSelect:) forControlEvents:UIControlEventTouchUpInside];
            [customPickerView addSubview:btn];
        } else {
            blur = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.width)];
            customPickerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.width - 162 - 44, _scrollView.frame.size.width, 206)];
            datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, _scrollView.frame.size.width, 162)];
            [customPickerView addSubview:datePicker];
            btn.frame = CGRectMake(0, 162, _scrollView.frame.size.width, 44);
            [btn addTarget:self action:@selector(timeSelect:) forControlEvents:UIControlEventTouchUpInside];
            [customPickerView addSubview:btn];
        }
        if (pickerType == 0) {
            // Date.
            datePicker.datePickerMode = UIDatePickerModeDate;
            if (ver_float < 7.0) {
                CALayer *mask = [[CALayer alloc] init];
                [mask setBackgroundColor: [UIColor whiteColor].CGColor];
                [mask setCornerRadius:5.0f];
                if (orientation == UIInterfaceOrientationPortrait) {
                    [mask setFrame:CGRectMake(24.0f, 10.0f, 272.0f, 196.0f)];
                } else {
                    if (_scrollView.frame.size.width == 480) {
                        [mask setFrame:CGRectMake(108.0f, 10.0f, _scrollView.frame.size.width - 216, 142.0f)];
                    } else {
                        [mask setFrame:CGRectMake(150.0f, 10.0f, 270.0f, 142.0f)];
                    }
                }
                [datePicker.layer setMask:mask];
            }
        } else {
            // Time.
            datePicker.datePickerMode = UIDatePickerModeTime;
            if (ver_float < 7.0) {
                CALayer *mask = [[CALayer alloc] init];
                [mask setBackgroundColor: [UIColor whiteColor].CGColor];
                [mask setCornerRadius:5.0f];
                if (orientation == UIInterfaceOrientationPortrait) {
                    [mask setFrame:CGRectMake(85.0f, 10.0f, 150.0f, 196.0f)];
                } else {
                    if (_scrollView.frame.size.width == 480) {
                        [mask setFrame:CGRectMake(160.0f, 10.0f, _scrollView.frame.size.width - 320, 142.0f)];
                    } else {
                        [mask setFrame:CGRectMake(200.0f, 10.0f, 164.0f, 142.0f)];
                    }
                }
                [datePicker.layer setMask:mask];
            }
        }
    } else {
        // Duration.
        hourArr = [[NSMutableArray alloc] init];
        minuteArr = [[NSMutableArray alloc] init];
        secondArr = [[NSMutableArray alloc] init];
        for (int i = 0; i <= 72; i++) {
            [hourArr addObject:[NSString stringWithFormat:@"%d", i]];
        }
        for (int i = 0; i <= 59; i++) {
            [minuteArr addObject:[NSString stringWithFormat:@"%d", i]];
            [secondArr addObject:[NSString stringWithFormat:@"%d", i]];
        }
        if (orientation == UIInterfaceOrientationPortrait) {
            blur = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
            customPickerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 216 - 44, 320, 260)];
            durationPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 320, 216)];
            durationPicker.delegate = self;
            [customPickerView addSubview:durationPicker];
            for (NSInteger i = 0; i < 3; i++) {
                UILabel *timeLabel = [[UILabel alloc] init];
                timeLabel.textColor = [UIColor blackColor];
                timeLabel.backgroundColor = [UIColor clearColor];
                if (i == 0) {
                    timeLabel.text = @"小時";
                } else if (i == 1) {
                    timeLabel.text = @"分鐘";
                } else {
                    timeLabel.text = @"秒";
                }
                if (ver_float < 7.0) {
                    timeLabel.frame = CGRectMake(70 + 100*i, 98, 44, 22);
                    [customPickerView addSubview:timeLabel];
                } else {
                    timeLabel.frame = CGRectMake(70 + 110*i, 98, 44, 22);
                    [durationPicker addSubview:timeLabel];
                }
                timeLabel = nil;
            }
            btn.frame = CGRectMake(0, 216, 320, 44);
            [btn addTarget:self action:@selector(durationTimeSelect:) forControlEvents:UIControlEventTouchUpInside];
            [customPickerView addSubview:btn];
            if (ver_float < 7.0) {
                CALayer *mask = [[CALayer alloc] init];
                [mask setBackgroundColor: [UIColor whiteColor].CGColor];
                [mask setCornerRadius:5.0f];
                [mask setFrame:CGRectMake(14.0f, 10.0f, 292.0f, 196.0f)];
                [durationPicker.layer setMask:mask];
            }
        } else {
            blur = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.width)];
            customPickerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.width - 162 - 44, _scrollView.frame.size.width, 206)];
            durationPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, _scrollView.frame.size.width, 162)];
            durationPicker.delegate = self;
            [customPickerView addSubview:durationPicker];
            CGRect screenBounds = [[UIScreen mainScreen] bounds];
            NSInteger basePosition;
            NSInteger adder;
            if (screenBounds.size.height == 480) {
                basePosition = 114;
                adder = 160;
            } else {
                basePosition = 122;
                adder = 188;
            }
            for (NSInteger i = 0; i < 3; i++) {
                UILabel *timeLabel = [[UILabel alloc] init];
                timeLabel.textColor = [UIColor blackColor];
                timeLabel.backgroundColor = [UIColor clearColor];
                if (i == 0) {
                    timeLabel.text = @"小時";
                } else if (i == 1) {
                    timeLabel.text = @"分鐘";
                } else {
                    timeLabel.text = @"秒";
                }
                if (ver_float < 7.0) {
                    timeLabel.frame = CGRectMake(basePosition + adder*i, 70, 44, 22);
                    [customPickerView addSubview:timeLabel];
                } else {
                    timeLabel.frame = CGRectMake(basePosition + adder*i, 70, 44, 22);
                    [durationPicker addSubview:timeLabel];
                }
                timeLabel = nil;
            }
            btn.frame = CGRectMake(0, 162, _scrollView.frame.size.width, 44);
            [btn addTarget:self action:@selector(durationTimeSelect:) forControlEvents:UIControlEventTouchUpInside];
            [customPickerView addSubview:btn];
            if (ver_float < 7.0) {
                CALayer *mask = [[CALayer alloc] init];
                [mask setBackgroundColor: [UIColor whiteColor].CGColor];
                [mask setCornerRadius:5.0f];
                [mask setFrame:CGRectMake(14.0f, 10.0f, _scrollView.frame.size.width - 30, 142.0f)];
                [durationPicker.layer setMask:mask];
            }
        }
    }
    blur.backgroundColor = [UIColor blackColor];
    blur.alpha = 0.5;
    [self.view addSubview:blur];
    customPickerView.backgroundColor = [UIColor whiteColor];
    customPickerView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    [self.view addSubview:customPickerView];
    [UIView animateWithDuration:0.3/1.5 animations:^{
        customPickerView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            customPickerView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3/2 animations:^{
                customPickerView.transform = CGAffineTransformIdentity;
            }];
        }];
    }];
}

- (void)timeSelect:(id)sender {
    [UIView animateWithDuration:0.3/1.5 animations:^{
        customPickerView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.8, 0.8);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            customPickerView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3/2 animations:^{
                customPickerView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
            } completion:^(BOOL finished) {
                [customPickerView removeFromSuperview];
                customPickerView = nil;
                [blur removeFromSuperview];
                blur = Nil;
            }];
        }];
    }];
    if (pickerType == 0) {
        NSDate *dateAnswer = datePicker.date;
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"YYYY/MM/dd"];
        NSString *dateStr = [dateFormat stringFromDate:dateAnswer];
        dateQ.answer = dateStr;
        dateQ.dateLabel.text = dateStr;
    } else {
        NSDate *timeAnswer = datePicker.date;
        NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
        [timeFormat setDateFormat:@"KK:mm aa"];
        NSString *timeStr = [timeFormat stringFromDate:timeAnswer];
        if (pickerType == 1) {
            dateQ.answer = [NSString stringWithFormat:@"%@-%@", dateQ.dateLabel.text, timeStr];
            dateQ.timeLabel.text = timeStr;
        } else { // 2.
            timeQ.answer = timeStr;
            timeQ.timeLabel.text = timeStr;
        }
    }
}

- (void)durationTimeSelect:(id)sender {
    [UIView animateWithDuration:0.3/1.5 animations:^{
        customPickerView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.8, 0.8);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            customPickerView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3/2 animations:^{
                customPickerView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
            } completion:^(BOOL finished) {
                [customPickerView removeFromSuperview];
                customPickerView = nil;
                [blur removeFromSuperview];
                blur = Nil;
            }];
        }];
    }];
    NSInteger hour = [durationPicker selectedRowInComponent:kHourComponent];
    NSInteger minute = [durationPicker selectedRowInComponent:kMinuteComponent];
    NSInteger second = [durationPicker selectedRowInComponent:kSecondComponent];
    NSString *displayTime = [NSString stringWithFormat:@"%@ 小時, %@ 分, %@ 秒",[hourArr objectAtIndex:hour],[minuteArr objectAtIndex:minute], [secondArr objectAtIndex:second]];
    NSString *answerTime = [NSString stringWithFormat:@"%@:%@:%@",[hourArr objectAtIndex:hour],[minuteArr objectAtIndex:minute], [secondArr objectAtIndex:second]];
    timeQ.timeLabel.text = displayTime;
    timeQ.answer = answerTime;
    hourArr = nil;
    minuteArr = nil;
    secondArr = nil;
}

#pragma mark PickerView
#define kHourComponent 0
#define kMinuteComponent 1
#define kSecondComponent 2

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    // picker 的內容數量
    if (component == kHourComponent)
        return [hourArr count];
    else if (component == kMinuteComponent)
        return [minuteArr count];
    else
        return [secondArr count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == kHourComponent)
        return [hourArr objectAtIndex:row];
    else if (component == kMinuteComponent)
        return [minuteArr objectAtIndex:row];
    else
        return [secondArr objectAtIndex:row];
}

#pragma Alert Related

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	UCAlertType tag = alertView.tag;
	if (tag == UCAlertTypeExit && buttonIndex != [alertView cancelButtonIndex]) {
		if (delegate) {
			[delegate UserCafeFailedWithError:UCUserBreak];
		}
		isFinish = NO;
        [self sendAnswer];
		// [self dismissViewControllerAnimated:YES completion:nil];
	}
	else if (tag == UCAlertTypeRequired) {
		CGFloat pageWidth = _scrollView.frame.size.width;
		CGRect backRect = CGRectMake(previousPage * pageWidth, 0, pageWidth, 100);
		[_scrollView scrollRectToVisible:backRect animated:NO];
		isDialogOpen = NO;
	}
}

#pragma mark Detect Device Orientation
- (void)detectDeviceOrientation {
    NSInteger width = _scrollView.frame.size.width;
    NSInteger height = _scrollView.frame.size.height;
    for (UIView * view in _scrollView.subviews) {
        [view removeFromSuperview];
    }
    visiblePageCount = 0;
    for (UCQuestion * q in questionArr) {
        visiblePageCount += q.show ? 1 : 0;
    }
    [_scrollView setContentSize:CGSizeMake(visiblePageCount * width, height)];
    NSUInteger offset = 0;
    NSInteger position = 0;
    for (UCQuestion * q in questionArr) {
        if (!q.show) {
            q.realPos = -1;
            continue;
        }
        q.realPos = offset;
        q.pos = [questionArr indexOfObject:q];
        UCQuestionViewController *tv = [[UCQuestionViewController alloc] initWithNibName:@"UCQuestionViewController" bundle:[UCContext getBundle]];
        [tv setFrame:CGRectMake(width * offset, 16, width, height)];
        position = [tv renderQuestion:q];
        [_scrollView addSubview:tv.view];
        if ([q isKindOfClass:[UCTextQuestion class]]) {
            q.position = position;
        }
        offset++;
    }
    [_scrollView setContentOffset:CGPointMake(previousPage * width, 0) animated:YES];
    orientation = [UIApplication sharedApplication].statusBarOrientation;
}

- (void)viewDidUnload {
    [self setAuthLable:nil];
    [super viewDidUnload];
}
@end
