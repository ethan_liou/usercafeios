//
//  UCQuestionViewController.h
//  UserCafeCore
//
//  Created by Liou Yu-Cheng on 13/3/4.
//  Copyright (c) 2013年 UserCafe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UCQuestion.h"
#import "UCPageBreakerQuestion.h"
/*
@protocol UCQuestionViewDelegate <NSObject>
- (void)breadcrumbTitle:(UCQuestion *)pagebreaker;
@end
 */

@interface UCQuestionViewController : UIViewController{
	IBOutlet UILabel * titleLabel;
	IBOutlet UILabel * requiredLabel;
	IBOutlet UILabel * descLabel;
	IBOutlet UIScrollView * scrollView;
    UIView *contentView;
    // IBOutlet 多了一個 button，用意是？
}

- (void)setFrame:(CGRect) rect;
- (NSInteger)renderQuestion:(UCQuestion *)question;

@end
