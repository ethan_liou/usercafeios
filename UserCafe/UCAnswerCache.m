//
//  UCAnswerCache.m
//  UserCafe
//
//  Created by Liou Yu-Cheng on 13/3/19.
//  Copyright (c) 2013年 usercare. All rights reserved.
//

#import "UCAnswerCache.h"

@implementation UCAnswerCache

@synthesize items;

- (void)initFMDBItem {
    fm = [NSFileManager defaultManager];
    paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    documentsDirectory = [paths objectAtIndex:0];
    writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"usercafe.sqlite"];
    success = [fm fileExistsAtPath:writableDBPath];
    
    if (!success) {
        NSError *error;
        NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"usercafe.sqlite"];
        success = [fm copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
        if (!success) {
            NSLog(@"error");
        }
    }
    
    // 連結資料庫
    db = [FMDatabase databaseWithPath:writableDBPath];
    if (![db open]) {
        NSLog(@"Could not open db");
    }
    else if ([db open]) {
        [db setShouldCacheStatements:YES];
        // Create the table
        if(![db executeUpdate:@"CREATE TABLE IF NOT EXISTS AnswerCache (_qid INTEGER NOT NULL, _key TEXT NOT NULL, _value TEXT NOT NULL)"])
        {
            NSLog(@"Could not create the table: %@", [db lastErrorMessage]);
        } else {
            NSLog(@"Create table success");
        }
    }
}

- (id)initWithMd5:(NSString *)md5
{
	self = [super init];
    // [self initFMDBItem];
	if (self) {
		_md5 = md5;
        // 它是 singleton（單例）
		NSDictionary *oriDict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:md5];
		if (oriDict != nil)
			_dict = [[NSMutableDictionary alloc] initWithDictionary:oriDict];
		else
			_dict = [[NSMutableDictionary alloc] init];
	}
	return self;
}

- (void)createNewCache{
    // FMDB
    /*
    success =  [db executeUpdate:@"DELETE FROM AnswerCache"];
    if (success) {
        NSLog(@"Create new AnswerCache table: success");
    } else {
        NSLog(@"Create new AnswerCache table: fail");
    }
     */
	[_dict removeAllObjects];
}

- (NSString *)loadCacheByName:(NSString *)name {
    /*
    FMResultSet *rs = [db executeQuery:@"SELECT _key, _value, FROM AnswerCache"];
    NSString *value;
    while ([rs next]) {
        NSString *key = [rs stringForColumn:@"_key"];
        if ([key isEqualToString:name]) {
            value = [rs stringForColumn:@"_value"];
        }
    }
    [rs close];
     */
//    NSLog(@"Load Cache By Name: %@, %@", value, [_dict objectForKey:name]);
	return [_dict objectForKey:name];
}

- (void)commit {
	[[NSUserDefaults standardUserDefaults] setObject:_dict forKey:_md5];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)saveCacheWithName:(NSString *)name AndAnswer:(NSString *)answer {
    /*
    // FMDB，Could not insert data: AnswerCache._qid may not be NULL
    if(![db executeUpdate:@"INSERT INTO AnswerCache (_key, _value) VALUES (?,?)", name, answer])
    {
        NSLog(@"Could not insert data: %@", [db lastErrorMessage]);
    }
     */
	[_dict setObject:answer forKey:name];
	[self commit];
}

@end
