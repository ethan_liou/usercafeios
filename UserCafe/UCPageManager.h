//
//  UCPageManager.h
//  UserCafeCore
//
//  Created by Liou Yu-Cheng on 13/3/4.
//  Copyright (c) 2013年 UserCafe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UCAnswerCache.h"
#import "UCMainViewController.h"

@interface UCPageManager : NSObject{
}

@property (nonatomic,strong) NSString * defaultActionUrl;
@property (nonatomic) NSInteger pageNum;
@property (nonatomic) NSInteger defaultNextPageNum;
@property (nonatomic) NSInteger mode;	//	0 : last, 1 : mid
@property (nonatomic) NSInteger jumpIdx;
@property (nonatomic,strong) NSArray * jumpPageNum;
@property (nonatomic,strong) NSMutableArray * questions;

@property (nonatomic) BOOL hidden;
@property (nonatomic) NSInteger nextPageNum;
@property (nonatomic) NSInteger prevPageNum;
@property (nonatomic) NSInteger googleVer;
@property (nonatomic, weak) UCMainViewController * owner;
// 以頁數為單位的 Dict
- (id)initWithDict:(NSDictionary *)json AndPageNum:(NSInteger)page AndVer:(NSInteger)ver;
- (NSDictionary*)getAnswerDict;

@end
