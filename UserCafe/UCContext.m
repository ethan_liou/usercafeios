//
//  UCContext.m
//  UserCafe
//
//  Created by Liou Yu-Cheng on 13/5/14.
//  Copyright (c) 2013年 usercare. All rights reserved.
//

#import "UCContext.h"
#import "UCUtils.h"
#import "UCAnswerCache.h"
#import "UCMainViewController.h"
#import "UCConstant.h"
#import <UIKit/UIKit.h>

@implementation UCContext

@synthesize formKey,account,customData,submitText;

static NSBundle * _bundle = nil;
static NSString * _sdk_key = nil;
static NSString * _device_id = nil;
static NSDictionary * _current_dict = nil;

- (id)init {
    self = [super init];
    if (self) {
		if (_bundle == nil) {
			[UCContext loadBundle:@"UserCafe.bundle"];
		}
    }
    return self;
}

+ (void)setSdkKey:(NSString*)key{
	_sdk_key = key;
	NSString *macaddress = [UCUtils getMacAddress];
	_device_id = [UCUtils calculateMd5ByData:[macaddress dataUsingEncoding:NSUTF8StringEncoding]];
}

+ (NSString*)getSdkKey{
	return _sdk_key;
}

+ (NSString*)getDeviceId{
	return _device_id;
}

+ (void)openQuestionnaire{
	UCContext * context = [[UCContext alloc] init];
	context.submitText = UCLocalizedString(@"http://usercafe.com", @"");
	context.customData = nil;

	UCMainViewController * controller = [[UCMainViewController alloc] initWithNibName:@"UCMainViewController" bundle:[UCContext getBundle]];
	controller.owner = context;
	controller.cacheKey = [_current_dict objectForKey:@"dev_key"];
	
	// find top view controller
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
	
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
	if (topController) {
		[topController presentViewController:controller animated:YES completion:nil];
	}
}

+ (UIView*)renderADBannerWithFrame:(CGRect)frame{
	_current_dict = [UCUtils loadPostDataRequestTo:[NSURL URLWithString:[NSString stringWithFormat:@"%@/get_paid_questionnaire",SERVER_URL]] WithDict:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:_sdk_key, _device_id, nil] forKeys:[NSArray arrayWithObjects:@"_sdk_key",@"_device_id", nil]]];
	if (_current_dict == nil) {
		return nil;
	}
	UIButton * btn = [[UIButton alloc] initWithFrame:frame];
	[btn setBackgroundColor:[UIColor blackColor]];
	[btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[btn.titleLabel setAdjustsFontSizeToFitWidth:YES];
	[btn setTitle:[_current_dict objectForKey:@"title"] forState:UIControlStateNormal];
	[btn addTarget:[UCContext class] action:@selector(openQuestionnaire) forControlEvents:UIControlEventTouchUpInside];
	return btn;
}

+ (void)clearCacheWithKey:(NSString *)key {
	UCAnswerCache *cache = [[UCAnswerCache alloc] initWithMd5:key];
	[cache createNewCache];
	[cache commit];
}

+ (NSBundle *)frameworkBundle:(NSString*)bundleName {
	static NSBundle* frameworkBundle = nil;
	static dispatch_once_t predicate;
	dispatch_once(&predicate, ^{
		NSString* mainBundlePath = [[NSBundle mainBundle] resourcePath];
		NSString* frameworkBundlePath = [mainBundlePath stringByAppendingPathComponent:bundleName];
		frameworkBundle = [NSBundle bundleWithPath:frameworkBundlePath];
	});
	return frameworkBundle;
}

+ (BOOL)loadBundle:(NSString*)bundleName{
	_bundle = [self frameworkBundle:bundleName];
	return _bundle != nil;
}

+ (NSBundle*)getBundle{
	return _bundle;
}

+ (UIImage*)getImage:(NSString*)name{
	name = [NSString stringWithFormat:@"%@.png",name];
	// Split into extension and name
	NSString *extension = [name pathExtension];
	NSAssert1(extension != nil, @"Invalid image name %@", name);
	name = [name stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@".%@", extension]
										   withString:@""
											  options:NSBackwardsSearch | NSAnchoredSearch
												range:NSMakeRange(0, [name length])];
	
	// Get scale
	UIScreen *screen = [UIScreen mainScreen];
	CGFloat scale = 1.0f;
	if ([screen respondsToSelector:@selector(scale)]) {
		scale = screen.scale;
	}
	
	// Transform into int
	NSUInteger intScale = (NSUInteger)round(scale);
	
	// Generate modified
	NSString *modifier = @"";
	if (intScale != 1) {
		modifier = [NSString stringWithFormat:@"@%dx", intScale];
	}
	
	// Generate resolution dependent name
	NSString *resolutionDependentName = [NSString stringWithFormat:@"%@%@", name, modifier];
		
	// Search for resolution dependent file in bundle
	NSString *path = [_bundle pathForResource:resolutionDependentName ofType:extension];
	if (path == nil) {
		// Not found, try to find standard res file
		path = [_bundle pathForResource:name ofType:extension];
	}
	
	if (path == nil) {
		// Still not found, return nil
		return nil;
	} else {
		// Load and return image
		return [UIImage imageWithContentsOfFile:path];
	}
}


@end
