//
//  UCPageBreakerQuestion.h
//  UserCafeCore
//
//  Created by Liou Yu-Cheng on 13/3/4.
//  Copyright (c) 2013年 UserCafe. All rights reserved.
//

#import "UCQuestion.h"

@interface UCPageBreakerQuestion : UCQuestion

-(id)initWithTitle:(NSString*)title AndDesc:(NSString*)desc AndRequired:(BOOL)required;
-(id)initWithDict:(NSDictionary*)dict WithOwner:(UCPageManager*)manager;

@end
