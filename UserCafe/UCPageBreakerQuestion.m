//
//  UCPageBreakerQuestion.m
//  UserCafeCore
//
//  Created by Liou Yu-Cheng on 13/3/4.
//  Copyright (c) 2013年 UserCafe. All rights reserved.
//

#import "UCPageBreakerQuestion.h"

@implementation UCPageBreakerQuestion

- (id)initWithTitle:(NSString *)title AndDesc:(NSString *)desc AndRequired:(BOOL)required
{
	self = [super init];
	if (self) {
		self.title = title;
		self.desc = desc;
		self.required = NO;
		self.show = YES;
	}
	return self;
}

- (id)initWithDict:(NSDictionary *)dict WithOwner:(UCPageManager *)manager{
	self = [super initWithDict:dict WithOwner:manager];
	if(self) {
        NSString *desc = [dict objectForKey:@"desc"];
        NSString *title = [dict objectForKey:@"title"];
        if (([desc isEqualToString:@""] && [title isEqualToString:@""])) {
            return nil;
        } else {
            self.required = NO;
            return self;
        }
	}
    return nil;
}

@end
