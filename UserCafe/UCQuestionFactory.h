//
//  UCQuestionFactory.h
//  UserCafeCore
//
//  Created by Liou Yu-Cheng on 13/3/4.
//  Copyright (c) 2013年 UserCafe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UCQuestion.h"
#import "UCPageManager.h"

@interface UCQuestionFactory : NSObject

+ (UCQuestion *)createQuestionWithDict:(NSDictionary*)dict WithOwner:(UCPageManager *)manager;

@end
