//
//  AbstractDeviceInfo.m
//  UserCafe
//
//  Created by Liou Yu-Cheng on 2013/10/14.
//  Copyright (c) 2013年 usercare. All rights reserved.
//

#import "AbstractDeviceInfo.h"

@implementation AbstractDeviceInfo

@synthesize ready,value;

-(id)init{
	self = [super init];
	if(self){
		self.ready = NO;
		self.value = nil;
	}
	return self;
}

-(NSString*) getName{
	@throw NSObjectInaccessibleException;
}

-(void) loadValue{
	@throw NSObjectInaccessibleException;
}

-(NSString*) getValue{
	if(!ready || value == nil || value.length == 0)
		return @"Unknown";
	else
		return value;
}

@end
