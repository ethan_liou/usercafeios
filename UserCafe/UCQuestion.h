//
//  UCQuestion.h
//  UserCafeCore
//
//  Created by Liou Yu-Cheng on 13/3/4.
//  Copyright (c) 2013年 UserCafe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "UCPageManager.h"
#import "UCStretchTextField.h"

typedef NS_ENUM(NSUInteger, UCQuestionType) {
    UCQuestionTypeText = 0,
    UCQuestionTypeLongText,
    UCQuestionTypeMultipleChoice,
    UCQuestionTypeCheckbox,
    UCQuestionTypeList,
    UCQuestionTypeScale,
    UCQuestionTypeGrid,
    UCQuestionTypePageBreaker,
    UCQuestionTypeSectionHeader,
    UCQuestionTypeSubmit,
    UCQuestionTypeImage,
    UCQuestionTypeDate,
    UCQuestionTypeTime,
};

@interface UCQuestion : NSObject

// from json
@property (nonatomic,strong) NSString * title;
@property (nonatomic,strong) NSString * desc;
@property (nonatomic) BOOL required;
@property (nonatomic) UCQuestionType type;
@property (nonatomic,strong) NSString * name;

// property
@property (nonatomic) BOOL show;
@property (nonatomic, strong) NSString * answer;
@property (nonatomic) BOOL firstQuestion;
@property (nonatomic) BOOL lastQuestion;
@property (nonatomic) NSUInteger pos;
@property (nonatomic) NSUInteger realPos;
@property (nonatomic, unsafe_unretained) UCPageManager * owner;

// layer
@property (nonatomic, strong) NSString * firstLayer;
@property (nonatomic, strong) NSString * secondLayer;
@property (nonatomic, strong) NSString * thirdLayer;

// other
@property (nonatomic, strong) NSString * otherString;
@property (nonatomic) BOOL useOther;
@property (nonatomic, strong) UCStretchTextField *theTextField;
@property (nonatomic) NSInteger position;
@property (nonatomic) NSInteger timer;

- (id)initWithDict:(NSDictionary*)dict WithOwner:(UCPageManager*)manager;
- (UIView*)renderContentView;
- (void)setHeight:(NSInteger)height ToView:(UIView*)view;

@end
