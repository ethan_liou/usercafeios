//
//  UCViewController.h
//  UserCafeCore
//
//  Created by Liou Yu-Cheng on 13/3/4.
//  Copyright (c) 2013年 UserCafe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserCafe.h"
#import "UCContext.h"
#import "BTBreadcrumbView.h"


@interface UCMainViewController : UIViewController <UIScrollViewDelegate, UIAlertViewDelegate, BTBreadcrumbViewDelegate, UIGestureRecognizerDelegate, UITextFieldDelegate, UIActionSheetDelegate, UIPickerViewDelegate, UIPickerViewDataSource>
{
	IBOutlet UIScrollView * _scrollView;
	IBOutlet UIButton * _prevBtn;
	IBOutlet UIButton * _nextBtn;
	IBOutlet UIProgressView * _progress;
	IBOutlet UIImageView * _navView;
	IBOutlet UIButton * _navBackBtn;
	NSDictionary * questionJson;
	NSMutableDictionary * pageNoToPageManagerDict;
	NSString * actionUrl;
	NSUInteger googleDocVer;
    
    UIView *blur;
    UIView *customPickerView;
    UIDatePicker *datePicker;
    UIPickerView *durationPicker;
    
    NSMutableArray *hourArr;
    NSMutableArray *minuteArr;
    NSMutableArray *secondArr;
    
    BTBreadcrumbView * _breadcrumb;
    NSString *tempTitle;
    BOOL breadcrumbCheckPoint;
}

@property (nonatomic, strong) NSString *cacheKey;
@property (nonatomic, strong) NSString *surveyKey;
@property (nonatomic, weak) id<UserCafeDelegate> delegate;
@property (nonatomic, assign) dispatch_queue_t queue;
@property (nonatomic, strong) UCContext * owner;
@property (weak, nonatomic) IBOutlet UILabel *authLable;
@property (weak, nonatomic) IBOutlet UIImageView *authImg;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
- (IBAction)navBtnClick:(id)sender;
- (BOOL)loadJson:(NSDictionary *)json;

typedef NS_OPTIONS(NSUInteger, UCAlertType) {
    UCAlertTypeRequired = (1UL << 0),
    UCAlertTypeExit = (1UL << 1)
};

@end
