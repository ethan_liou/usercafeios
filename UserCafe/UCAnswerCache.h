//
//  UCAnswerCache.h
//  UserCafe
//
//  Created by Liou Yu-Cheng on 13/3/19.
//  Copyright (c) 2013年 usercare. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"

@interface UCAnswerCache : NSObject {
	NSString * _md5;
	NSMutableDictionary * _dict;
    
    // FMDB
    FMDatabase *db;
    BOOL success;
    NSFileManager *fm;
    NSArray *paths;
    NSString *documentsDirectory;
    NSString *writableDBPath;
}
// FMDB
@property (strong, nonatomic) NSMutableArray *items;


- (id)initWithMd5:(NSString *)md5;
- (void)createNewCache;
- (void)saveCacheWithName:(NSString *)name AndAnswer:(NSString *)answer;
- (NSString*)loadCacheByName:(NSString *)name;
- (void)commit;

@end
