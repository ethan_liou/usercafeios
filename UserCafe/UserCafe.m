//
//  UserCafe.m
//  UserCafe
//
//  Created by Liou Yu-Cheng on 13/3/18.
//  Copyright (c) 2013年 usercare. All rights reserved.
//

#import "UserCafe.h"
#import "UCMainViewController.h"
#import "UCUtils.h"
#import "UCConstant.h"
#import "UCContext.h"
#import "UCAnswerCache.h"


@interface UserCafe ()

@end

@implementation UserCafe

+ (void)showUserCafeWithDelegate:(id<UserCafeDelegate>)delegate
                          appKey:(NSString *)key
                       surveyKey:(NSString *)sKey
                      submitText:(NSString *)text
                      clearCache:(BOOL)clear
                      customData:(NSDictionary *)data
{
	// Set context.
	UCContext *context = [[UCContext alloc] init];
	context.submitText = text;
	context.customData = data;
	
	if([UCContext getBundle] == nil) {
		if (delegate)
			[delegate UserCafeFailedWithError:UCNoBundle];
		return;
	}
	// Load data.
	if (clear) {
		[UCContext clearCacheWithKey:key];
	}
	
	UCMainViewController *controller = [[UCMainViewController alloc] initWithNibName:@"UCMainViewController" bundle:[UCContext getBundle]];
	controller.delegate = delegate;
	controller.cacheKey = key;
	controller.owner = context;
    controller.surveyKey = sKey;

	// Find top view controller.
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
	if (topController) {
		[topController presentViewController:controller animated:YES completion:nil];
	}
}

@end
