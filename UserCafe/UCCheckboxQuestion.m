//
//  UCCheckboxQuestion.m
//  UserCafeCore
//
//  Created by Liou Yu-Cheng on 13/3/4.
//  Copyright (c) 2013年 UserCafe. All rights reserved.
//

#import "UCCheckboxQuestion.h"
#import "UCContext.h"

@implementation UCCheckboxQuestion

-(BOOL)isOtherOpt:(NSString*)opt ByVer:(NSInteger) version{
	BOOL ret = NO;
	switch (version) {
		case 1:
			ret = [opt isEqualToString:@"__option__"];
			break;
		case 2:
			ret = [opt isEqualToString:@"__other_option__"];
			break;
		default:
			break;
	}
	return ret;
}

- (id)initWithDict:(NSDictionary*)dict WithOwner:(UCPageManager*)manager{
	self = [super initWithDict:dict WithOwner:manager];
	if(self){
		self.options = [[NSArray alloc] initWithArray:[dict objectForKey:@"options"]];
		checkSet = [[NSMutableSet alloc] initWithCapacity:self.options.count];
	}
	return self;
}

- (void)changeAnswer:(NSString *)option Choose:(BOOL)choose {
	if (choose) {
		[checkSet addObject:option];
	}
	else {
		[checkSet removeObject:option];
	}
	self.answer = [[checkSet allObjects] componentsJoinedByString:@"\n"];
	if (self.answer.length == 0) {
		self.answer = nil;
	}
}

- (UIView*)renderContentView {
    UIView *rootView;
    UIInterfaceOrientation orientation;
    orientation = [UIApplication sharedApplication].statusBarOrientation;
    if(orientation == UIInterfaceOrientationPortrait) {
        rootView = [[[UCContext getBundle] loadNibNamed:@"MultipleChoiceQuestion" owner:self options:nil] objectAtIndex:0];
    } else {
        CGRect screenBounds = [[UIScreen mainScreen] bounds];
        if (screenBounds.size.height == 480) {
            rootView = [[[UCContext getBundle] loadNibNamed:@"MultipleChoiceQuestionLand" owner:self options:nil] objectAtIndex:0];
        } else {
            rootView = [[[UCContext getBundle] loadNibNamed:@"MultipleChoiceQuestionLand4" owner:self options:nil] objectAtIndex:0];
        }
    }
	for (UIView * v in rootView.subviews) {
		if([v isKindOfClass:[UITableView class]]){
			tableView_ = (UITableView *)v;
			if(self.answer != nil){
				NSArray * answers = [self.answer componentsSeparatedByString:@"\n"];
				for (NSString * ans in answers) {
					NSInteger idx = [self.options indexOfObject:ans];
					if(idx != NSNotFound)
						[checkSet addObject:ans];
				}
			}
			tableView_.dataSource = self;
			tableView_.delegate = self;
			// set height
			NSUInteger height = 30 + 24;
			for(int i = 0 ; i < self.options.count ; i++){
				NSIndexPath * index = [NSIndexPath indexPathForRow:i inSection:0];
				height += [self tableView:tableView_ heightForRowAtIndexPath:index];
			}
			[self setHeight:height ToView:rootView];
		}
	}
	return rootView;
}

-(void)setCheck:(BOOL)check toCell:(UITableViewCell*)cell{
	NSString * imageName;
	if(check){
		imageName = @"checkbox_select";
	}
	else{
		imageName = @"checkbox_unselect";
	}
	UIImageView * iv = [[UIImageView alloc] initWithImage:[UCContext getImage:imageName]];
	[cell setAccessoryView:iv];
}

#pragma UITableView data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return self.options.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	static NSString * reuseIdentifier = @"Cell";
	UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
	
	cell.textLabel.numberOfLines = 0;
	cell.textLabel.font = [UIFont systemFontOfSize:17];
	
	NSString * opt = [self.options objectAtIndex:indexPath.row];
	if([self isOtherOpt:opt ByVer:self.owner.googleVer]){
		opt = @"其他";
		otherIndexPath_ = indexPath;
	}
	cell.textLabel.text = opt;
	
	[self setCheck:[checkSet containsObject:opt] toCell:cell];
	cell.tag = 0;
	return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	CGSize size = [[self.options objectAtIndex:indexPath.row]
                   sizeWithFont:[UIFont systemFontOfSize:17]
                   constrainedToSize:CGSizeMake(240, CGFLOAT_MAX)];
    return size.height + 24;
}

#pragma UITableView delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
	NSString * option = [self.options objectAtIndex:indexPath.row];
	if (cell.tag == 0) {
		// uncheck -> check
		[self setCheck:YES toCell:cell];
		cell.tag = 1;
		[self changeAnswer:option Choose:YES];
	} else {
		// check -> uncheck
		[self setCheck:NO toCell:cell];
		cell.tag = 0;
		[self changeAnswer:option Choose:NO];
	}
	
	if([self isOtherOpt:option ByVer:self.owner.googleVer]){
		self.useOther = cell.tag == 1;
		// other
		UIAlertView* alert = [[UIAlertView alloc] initWithTitle:self.title message:@"" delegate:self cancelButtonTitle:UCLocalizedString(@"cancel", @"取消") otherButtonTitles:UCLocalizedString(@"ok", @"確定"), nil];
		alert.alertViewStyle = UIAlertViewStylePlainTextInput;
		alert.delegate = self;
		otherTextField_ = [alert textFieldAtIndex:0];
		otherTextField_.text = self.otherString == nil ? @"" : self.otherString;
		otherTextField_.keyboardAppearance = UIKeyboardAppearanceAlert;
		[otherTextField_ becomeFirstResponder];
		[alert show];
	}
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	if(buttonIndex != alertView.cancelButtonIndex && otherTextField_ != nil && otherIndexPath_ != nil){
		// find other cell
		UITableViewCell* cell = [tableView_ cellForRowAtIndexPath:otherIndexPath_];
		self.otherString = otherTextField_.text;
		cell.textLabel.text = [NSString stringWithFormat:UCLocalizedString(@"other", @"其他: %@"),otherTextField_.text];
	}
}


@end
