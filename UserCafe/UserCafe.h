//
//  UserCafe.h
//  UserCafe
//
//  Created by Liou Yu-Cheng on 13/3/18.
//  Copyright (c) 2013年 usercare. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, UCErrorCode) {
    UCNetworkError = 0,
    UCUserBreak = 1,
    UCNoBundle = 2,
	UCFilled = 3
};

@protocol UserCafeDelegate <NSObject>

@optional
- (void)UserCafeSuccess;
- (void)UserCafeFailedWithError:(UCErrorCode) error;
@end

@interface UserCafe : NSObject

+ (void)showUserCafeWithDelegate:(id<UserCafeDelegate>)delegate
                          appKey:(NSString*)key
                       surveyKey:(NSString *)sKey
                      submitText:(NSString*)text
                      clearCache:(BOOL)clear
                      customData:(NSDictionary*)data;

@end

