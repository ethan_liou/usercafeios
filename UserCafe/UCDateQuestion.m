//
//  UCDateQuestion.m
//  UserCafe
//
//  Created by 威威 on 13/7/16.
//  Copyright (c) 2013年 usercare. All rights reserved.
//

#import "UCDateQuestion.h"
#import <QuartzCore/QuartzCore.h>

@implementation UCDateQuestion
@synthesize delegate, dateType;
- (IBAction)dateBtn:(id)sender {
    [delegate showTimePicker:0];
}
- (IBAction)timeBtn:(id)sender {
    [delegate showTimePicker:1];
}
- (IBAction)dateBtnLand:(id)sender {
    [delegate showTimePicker:0];
}
- (IBAction)timeBtnLand:(id)sender {
    [delegate showTimePicker:1];
}

- (id)initWithDict:(NSDictionary *)dict WithOwner:(UCPageManager *)manager
{
	self = [super initWithDict:dict WithOwner:manager];
	if (self) {
        dateType = [[dict objectForKey:@"date_type"] integerValue];
	}
	return self;
}

- (UIView *)renderContentView
{
    UIView *rootView;
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    if(orientation == UIInterfaceOrientationPortrait) {
        rootView = [[[UCContext getBundle] loadNibNamed:@"UCDateQuestion" owner:self options:nil] objectAtIndex:0];
    }
    else if(orientation == UIInterfaceOrientationLandscapeRight || orientation == UIInterfaceOrientationLandscapeLeft) {
        CGRect screenBounds = [[UIScreen mainScreen] bounds];
        if (screenBounds.size.height == 480) {
            rootView = [[[UCContext getBundle] loadNibNamed:@"UCDateQuestionLand" owner:self options:nil] objectAtIndex:0];
        } else {
            rootView = [[[UCContext getBundle] loadNibNamed:@"UCDateQuestionLand4" owner:self options:nil] objectAtIndex:0];
        }
    }
    
    rootView.backgroundColor = [UIColor colorWithRed:227.0/255.0 green:227.0/255.0 blue:227.0/255.0 alpha:1];
    UITapGestureRecognizer *tapDateRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleUILabelDateTap:)];
    tapDateRecognizer.numberOfTapsRequired = 1;
    tapDateRecognizer.numberOfTouchesRequired = 1;
    tapDateRecognizer.cancelsTouchesInView = NO;
    tapDateRecognizer.delegate = self;
    
    UITapGestureRecognizer *tapTimeRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleUILabelTimeTap:)];
    tapTimeRecognizer.numberOfTapsRequired = 1;
    tapTimeRecognizer.numberOfTouchesRequired = 1;
    tapTimeRecognizer.cancelsTouchesInView = NO;
    tapTimeRecognizer.delegate = self;
    
    if (dateType == 0 || dateType == 2) {
        // only date.
        self.timeLabel.hidden = YES;
        self.timeBtn.hidden = YES;
        if (self.answer) {
            self.dateLabel.text = self.answer;
        } else {
            // no cache.
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            NSDate *date = [NSDate date];
            [formatter setDateFormat:@"YYYY/MM/dd"];
            self.dateLabel.text = [formatter stringFromDate:date];
        }
        self.dateLabel.tag = DATE_TAG;
        self.dateLabel.userInteractionEnabled = YES;
        [self.dateLabel addGestureRecognizer:tapDateRecognizer];
    } else {
        if (self.answer) {
            NSArray *cut = [self.answer componentsSeparatedByString:@"-"];
            self.dateLabel.text = [cut objectAtIndex:0];
            if (cut.count == 1) {
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                NSDate *date = [NSDate date];
                [formatter setDateFormat:@"KK:mm aa"];
                self.timeLabel.text = [formatter stringFromDate:date];
            } else if (cut.count == 2) {
                self.timeLabel.text = [cut objectAtIndex:1];
            }
        } else {
            // no cache.
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            NSDate *date = [NSDate date];
            [formatter setDateFormat:@"YYYY/MM/dd"];
            self.dateLabel.text = [formatter stringFromDate:date];
            [formatter setDateFormat:@"KK:mm aa"];
            self.timeLabel.text = [formatter stringFromDate:date];
        }
        self.dateLabel.tag = DATE_TAG;
        self.timeLabel.tag = TIME_TAG;
        self.timeLabel.userInteractionEnabled = YES;
        self.dateLabel.userInteractionEnabled = YES;
        [self.dateLabel addGestureRecognizer:tapDateRecognizer];
        [self.timeLabel addGestureRecognizer:tapTimeRecognizer];
    }
    return rootView;
}

- (void)handleUILabelDateTap:(UITapGestureRecognizer *)recognizer {
    [delegate showTimePicker:0];
}

- (void)handleUILabelTimeTap:(UITapGestureRecognizer *)recognizer {
    [delegate showTimePicker:1];
}
@end
