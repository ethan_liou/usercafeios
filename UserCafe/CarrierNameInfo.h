//
//  CarrierNameInfo.h
//  UserCafe
//
//  Created by Liou Yu-Cheng on 2013/10/14.
//  Copyright (c) 2013年 usercare. All rights reserved.
//

#import "AbstractDeviceInfo.h"

@interface CarrierNameInfo : AbstractDeviceInfo

@end
