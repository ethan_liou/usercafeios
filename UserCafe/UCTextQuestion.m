//
//  UCTextQuestion.m
//  UserCafeCore
//
//  Created by Liou Yu-Cheng on 13/3/4.
//  Copyright (c) 2013年 UserCafe. All rights reserved.
//

#import "UCTextQuestion.h"
#import "UCStretchTextField.h"
#import "UCContext.h"
#import "UCConstant.h"

@implementation UCTextQuestion

- (id)initWithDict:(NSDictionary*)dict WithOwner:(UCPageManager*)manager {
	self = [super initWithDict:dict WithOwner:manager];
	if (self) {
		
	}
	return self;
}

- (void)textFieldDidChange:(UITextField *)field{
	if (field.text.length == 0)
		self.answer = nil;
	else
		self.answer = field.text;
}

- (UIView *)renderContentView {
    UIView *rootView;
    UIInterfaceOrientation orientation;
    orientation = [UIApplication sharedApplication].statusBarOrientation;
    if(orientation == UIInterfaceOrientationPortrait) {
        rootView = [[[UCContext getBundle] loadNibNamed:@"TextQuestion" owner:self options:nil] objectAtIndex:0];
    }
    else if(orientation == UIInterfaceOrientationLandscapeRight || orientation == UIInterfaceOrientationLandscapeLeft) {
        rootView = [[[UCContext getBundle] loadNibNamed:@"TextQuestionLand" owner:self options:nil] objectAtIndex:0];
    }
	for (UIView * v in rootView.subviews) {
		if ([v isKindOfClass:[UCStretchTextField class]]) {
			UCStretchTextField *tv = (UCStretchTextField *)v;
			[tv removeTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
			[tv addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
			tv.delegate = self;
            self.theTextField = tv;
			v.tag = TEXT_TAG_OFFSET + self.pos;
			if(self.answer != nil){
				tv.text = self.answer;
			}
		}
	}
	return rootView;
}

#pragma mark UCTextQuestion

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
	[[NSNotificationCenter defaultCenter] postNotificationName:NOTI_NEXTPAGE object:nil];
	return YES;
}

@end
