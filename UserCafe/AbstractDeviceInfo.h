//
//  AbstractDeviceInfo.h
//  UserCafe
//
//  Created by Liou Yu-Cheng on 2013/10/14.
//  Copyright (c) 2013年 usercare. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AbstractDeviceInfo : NSObject

-(NSString*) getName;
-(void) loadValue;
-(NSString*) getValue;

@property (nonatomic, strong) NSString * value;
@property (nonatomic) BOOL ready;

-(id)init;

@end
