//
//  HTMLUtils.h
//  UserCafe
//
//  Created by Liou Yu-Cheng on 13/3/31.
//  Copyright (c) 2013年 usercare. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTMLUtils : NSObject

+ (NSString *)stringByUnescapingFromHTML:(NSString*)str;
+(NSString*) dictToString:(NSDictionary*)params;

@end
