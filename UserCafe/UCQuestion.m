//
//  UCQuestion.m
//  UserCafeCore
//
//  Created by Liou Yu-Cheng on 13/3/4.
//  Copyright (c) 2013年 UserCafe. All rights reserved.
//

#import "UCQuestion.h"

@implementation UCQuestion

@synthesize title, desc, required, type, name;
@synthesize answer, firstQuestion, lastQuestion, pos, realPos, owner, show;
@synthesize firstLayer, secondLayer, thirdLayer;
@synthesize otherString, useOther;

- (id)initWithDict:(NSDictionary *)dict WithOwner:(UCPageManager*)manager{
	self = [super init];
	if (self) {
		// common
		self.desc = [dict objectForKey:@"desc"];
		self.required = ((NSNumber*)[dict objectForKey:@"required"]).integerValue != 0;
		self.title = [dict objectForKey:@"title"];
		self.type = (UCQuestionType)((NSNumber*)[dict objectForKey:@"type"]).integerValue;
		self.name = [dict objectForKey:@"name"];
		self.owner = manager;
		// init attr
		self.answer = nil;
		self.show = YES;
	}
	return self;
}

- (void)setHeight:(NSInteger)height ToView:(UIView *)view {
	NSInteger oriHeight = view.frame.size.height;
	height = height > oriHeight ? height : oriHeight;
	[view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, height)];

}

- (UIView *)renderContentView{
//	@throw [NSException exceptionWithName:@"NeedImplement" reason:@"Implement it!" userInfo:nil];
    UIView *v;
    UIInterfaceOrientation orientation;
    orientation = [UIApplication sharedApplication].statusBarOrientation;
    if(orientation == UIInterfaceOrientationPortrait) {
        v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    }
    else if(orientation == UIInterfaceOrientationLandscapeRight || orientation == UIInterfaceOrientationLandscapeLeft) {
        v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 480, 44)];
    }
    return v;
}

@end
