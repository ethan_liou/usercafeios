//
//  UCMultipleChoiceQuestion.m
//  UserCafeCore
//
//  Created by Liou Yu-Cheng on 13/3/4.
//  Copyright (c) 2013年 UserCafe. All rights reserved.
//

#import "UCMultipleChoiceQuestion.h"
#import "UCConstant.h"
#import "UCContext.h"

@implementation UCMultipleChoiceQuestion

@synthesize jumpPage;

- (BOOL)isOtherOpt:(NSString*)opt ByVer:(NSInteger) version{
	BOOL ret = NO;
	switch (version) {
		case 1:
			ret = [opt isEqualToString:@"__option__"];
			break;
		case 2:
			ret = [opt isEqualToString:@"__other_option__"];
			break;
		default:
			break;
	}
	return ret;
}

- (void)setNextPage
{
	if (self.jumpPage) {
		NSInteger selectedIdx = [self.options indexOfObject:self.answer];
		if (selectedIdx == NSNotFound)
			return;
		// set next page
		NSUInteger nextPage = ((NSNumber *)[self.owner.jumpPageNum objectAtIndex:selectedIdx]).unsignedIntegerValue;
		self.owner.nextPageNum = nextPage;
		NSArray *array = [NSArray arrayWithObjects:
						   [NSNumber numberWithUnsignedInteger:self.owner.pageNum],
						   [NSNumber numberWithUnsignedInteger:nextPage], nil];
		[[NSNotificationCenter defaultCenter] postNotificationName:NOTI_JUMPPAGE object:array];
	}
}

- (id)initWithTitle:(NSString *)title AndName:(NSString *)name AndDesc:(NSString *)desc AndRequired:(BOOL)required AndOptions:(NSArray *)options
{
	self = [super init];
	if(self) {
		self.title = title;
		self.name = name;
		self.desc = desc;
		self.required = required;
		self.options = options;
		self.show = YES;
	}
	return self;

}

- (id)initWithDict:(NSDictionary*)dict WithOwner:(UCPageManager*)manager{
	self = [super initWithDict:dict WithOwner:manager];
	if(self){
		self.options = [[NSArray alloc] initWithArray:[dict objectForKey:@"options"]];
	}
	return self;
}

- (UIView *)renderContentView
{
    UIView *rootView;
    UIInterfaceOrientation orientation;
    orientation = [UIApplication sharedApplication].statusBarOrientation;
    if(orientation == UIInterfaceOrientationPortrait) {
        rootView = [[[UCContext getBundle] loadNibNamed:@"MultipleChoiceQuestion" owner:self options:nil] objectAtIndex:0];
    } else {
        CGRect screenBounds = [[UIScreen mainScreen] bounds];
        if (screenBounds.size.height == 480) {
            rootView = [[[UCContext getBundle] loadNibNamed:@"MultipleChoiceQuestionLand" owner:self options:nil] objectAtIndex:0];
        } else {
            rootView = [[[UCContext getBundle] loadNibNamed:@"MultipleChoiceQuestionLand4" owner:self options:nil] objectAtIndex:0];
        }
    }
	for (UIView * v in rootView.subviews) {
		if([v isKindOfClass:[UITableView class]]){
			tableView_ = (UITableView *)v;
			if (self.answer != nil) {
				NSInteger idx = [self.options indexOfObject:self.answer];
				if(idx != NSNotFound){
					checkedIndexPath = [NSIndexPath indexPathForRow:idx inSection:0];
				}
			}
			tableView_.dataSource = self;
			tableView_.delegate = self;
			// set height
			NSUInteger height = 30 + 24;
			for(int i = 0 ; i < self.options.count ; i++){
				NSIndexPath * index = [NSIndexPath indexPathForRow:i inSection:0];
				height += [self tableView:tableView_ heightForRowAtIndexPath:index];
			}
			[self setHeight:height ToView:rootView];
		}
	}
	return rootView;
}

#pragma UITableView data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return self.options.count;
}

- (void)setCheck:(BOOL)check toCell:(UITableViewCell*)cell
{
	NSString * imageName;
	if (check) {
		imageName = @"select";
	}
	else {
		imageName = @"unselect";
	}
	UIImageView * iv = [[UIImageView alloc] initWithImage:[UCContext getImage:imageName]];
	[cell setAccessoryView:iv];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString * reuseIdentifier = @"Cell";
	UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
	
	if(checkedIndexPath && [checkedIndexPath isEqual:indexPath]){
		[self setCheck:YES toCell:cell];
	}
	else{
		[self setCheck:NO toCell:cell];
	}
	cell.textLabel.numberOfLines = 0;
	cell.textLabel.font = [UIFont systemFontOfSize:17];
	NSString * opt = [self.options objectAtIndex:indexPath.row];
	cell.tag = 0;
	if([self isOtherOpt:opt ByVer:self.owner.googleVer]){
		opt = @"其他";
		cell.tag = 1;
	}
	cell.textLabel.text = opt;
	return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
	CGSize size = [[self.options objectAtIndex:indexPath.row]
                   sizeWithFont:[UIFont systemFontOfSize:17]
                   constrainedToSize:CGSizeMake(240, CGFLOAT_MAX)];
    return size.height + 24;
}

#pragma UITableView delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (checkedIndexPath) {
        UITableViewCell* uncheckCell = [tableView
										cellForRowAtIndexPath:checkedIndexPath];

		[self setCheck:NO toCell:uncheckCell];
    }
	UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
	if (cell.tag == 1) {
		// other
		UIAlertView* alert = [[UIAlertView alloc] initWithTitle:self.title message:@"" delegate:self cancelButtonTitle:UCLocalizedString(@"cancel", @"取消") otherButtonTitles:UCLocalizedString(@"ok", @"取消"), nil];
		alert.delegate = self;
		alert.alertViewStyle = UIAlertViewStylePlainTextInput;
		otherTextField_ = [alert textFieldAtIndex:0];
		otherTextField_.text = self.otherString == nil ? @"" : self.otherString;
		otherTextField_.keyboardAppearance = UIKeyboardAppearanceAlert;
		[otherTextField_ becomeFirstResponder];
		[alert addSubview:otherTextField_];
		[alert show];
	}
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
	[self setCheck:YES toCell:cell];
    checkedIndexPath = indexPath;
	if (cell.tag) {
		// other
		self.useOther = YES;
	}
	else{
		self.useOther = NO;
	}
	self.answer = [self.options objectAtIndex:indexPath.row];
	[self setNextPage];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	if(buttonIndex != alertView.cancelButtonIndex && checkedIndexPath != nil && otherTextField_ != nil){
		// find other cell
		UITableViewCell* cell = [tableView_ cellForRowAtIndexPath:checkedIndexPath];
		if(cell.tag){
			self.otherString = otherTextField_.text;
			cell.textLabel.text = [NSString stringWithFormat:UCLocalizedString(@"other", @"其他: %@"),otherTextField_.text];
		}
	}
}

@end
