//
//  UCGridQuestion.m
//  UserCafeCore
//
//  Created by Liou Yu-Cheng on 13/3/4.
//  Copyright (c) 2013年 UserCafe. All rights reserved.
//

#import "UCGridQuestion.h"
#import "UCPageBreakerQuestion.h"
#import "UCMultipleChoiceQuestion.h"

@implementation UCGridQuestion

- (id)initWithDict:(NSDictionary *)dict WithOwner:(UCPageManager*)manager
{
	self = [super initWithDict:dict WithOwner:manager];
	if (self) {
		scoreLabels = [[NSArray alloc] initWithArray:[dict objectForKey:@"score_labels"]];
		rowLabels = [[NSArray alloc] initWithArray:[dict objectForKey:@"row_labels"]];
		names = [self.name componentsSeparatedByString:@"&"];
	}
	return self;
}

-(void)addFirstPageAndMultipleQuestionToArr:(NSMutableArray *)array{
	// add page breaker
	UCPageBreakerQuestion *pageBreaker = [[UCPageBreakerQuestion alloc] initWithTitle:self.title AndDesc:self.desc AndRequired:self.required];
    pageBreaker.type = 8;
    pageBreaker.firstLayer = pageBreaker.title;
	[array addObject:pageBreaker];
	
	// add multiple question
	NSUInteger offset = 0;
	for (NSString* rowLabel in rowLabels) {
		UCMultipleChoiceQuestion *multiQuestion = [[UCMultipleChoiceQuestion alloc] initWithTitle:rowLabel AndName:[names objectAtIndex:offset] AndDesc:@"" AndRequired:self.required AndOptions:scoreLabels];
        multiQuestion.firstLayer = pageBreaker.title;
        multiQuestion.secondLayer = multiQuestion.title;
		[array addObject:multiQuestion];
		offset++;
	}
}

@end
